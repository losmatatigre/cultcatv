<?php

/**
 * This is the model class for table "public.document".
 *
 * The followings are the available columns in table 'public.document':
 * @property string $acceso
 * @property string $ext_acceso
 * @property string $jerarquia
 * @property string $ubicacion
 * @property string $tipo_liter
 * @property string $nivel_bibl
 * @property string $nivel_reg
 * @property string $tipo_ref_analitica
 * @property string $paginas
 * @property string $volumen
 * @property string $volumen_final
 * @property string $numero
 * @property string $numero_final
 * @property string $numeracion
 * @property string $enum_nivel_ini_3
 * @property string $enum_nivel_ini_4
 * @property string $enum_nivel_fin_3
 * @property string $enum_nivel_fin_4
 * @property string $cod_editor
 * @property string $cod_editor_inst
 * @property string $edicion
 * @property string $fecha_iso
 * @property string $fecha_iso_final
 * @property string $fecha_pub
 * @property string $isbn
 * @property string $indice_suplemento
 * @property string $cod_conf
 * @property string $cod_proy
 * @property string $disemina
 * @property string $url
 * @property string $n_disk_cin
 * @property string $sala
 * @property string $impresion
 * @property string $orden
 * @property string $cerrado
 * @property string $hora_iso_inicio
 * @property string $hora_iso_final
 *
 * The followings are the available model relations:
 * @property Central $acceso0
 * @property Central $extAcceso
 * @property Conferen $codConf
 * @property Editor $codEditor
 * @property EditorInst $codEditorInst
 * @property Proyecto $codProy
 * @property Tipodoc $tipoLiter
 */
class Document extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acceso, ext_acceso, jerarquia', 'required'),
			array('acceso', 'length', 'max'=>13),
			array('ext_acceso, n_disk_cin, orden', 'length', 'max'=>4),
			array('jerarquia, volumen, volumen_final, numero, numero_final, enum_nivel_ini_3, enum_nivel_ini_4, enum_nivel_fin_3, enum_nivel_fin_4, fecha_iso, fecha_iso_final, hora_iso_inicio, hora_iso_final', 'length', 'max'=>8),
			array('ubicacion, paginas, disemina', 'length', 'max'=>50),
			array('tipo_liter, nivel_reg', 'length', 'max'=>3),
			array('nivel_bibl, cerrado', 'length', 'max'=>1),
			array('tipo_ref_analitica', 'length', 'max'=>2),
			array('numeracion', 'length', 'max'=>64),
			array('cod_editor, cod_editor_inst, cod_conf, cod_proy', 'length', 'max'=>6),
			array('edicion', 'length', 'max'=>100),
			array('fecha_pub', 'length', 'max'=>80),
			array('isbn', 'length', 'max'=>20),
			array('indice_suplemento', 'length', 'max'=>32),
			array('url', 'length', 'max'=>255),
			array('sala', 'length', 'max'=>35),
			array('impresion', 'length', 'max'=>25),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('acceso, ext_acceso, jerarquia, ubicacion, tipo_liter, nivel_bibl, nivel_reg, tipo_ref_analitica, paginas, volumen, volumen_final, numero, numero_final, numeracion, enum_nivel_ini_3, enum_nivel_ini_4, enum_nivel_fin_3, enum_nivel_fin_4, cod_editor, cod_editor_inst, edicion, fecha_iso, fecha_iso_final, fecha_pub, isbn, indice_suplemento, cod_conf, cod_proy, disemina, url, n_disk_cin, sala, impresion, orden, cerrado, hora_iso_inicio, hora_iso_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'acceso0' => array(self::BELONGS_TO, 'Central', 'acceso'),
			'extAcceso' => array(self::BELONGS_TO, 'Central', 'ext_acceso'),
			'codConf' => array(self::BELONGS_TO, 'Conferen', 'cod_conf'),
			'codEditor' => array(self::BELONGS_TO, 'Editor', 'cod_editor'),
			'codEditorInst' => array(self::BELONGS_TO, 'EditorInst', 'cod_editor_inst'),
			'codProy' => array(self::BELONGS_TO, 'Proyecto', 'cod_proy'),
			'tipoLiter' => array(self::BELONGS_TO, 'Tipodoc', 'tipo_liter'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'acceso' => 'Acceso',
			'ext_acceso' => 'Ext Acceso',
			'jerarquia' => 'Jerarquia',
			'ubicacion' => 'Ubicacion',
			'tipo_liter' => 'Tipo Liter',
			'nivel_bibl' => 'Nivel Bibl',
			'nivel_reg' => 'Nivel Reg',
			'tipo_ref_analitica' => 'Tipo Ref Analitica',
			'paginas' => 'Paginas',
			'volumen' => 'Volumen',
			'volumen_final' => 'Volumen Final',
			'numero' => 'Numero',
			'numero_final' => 'Numero Final',
			'numeracion' => 'Numeracion',
			'enum_nivel_ini_3' => 'Enum Nivel Ini 3',
			'enum_nivel_ini_4' => 'Enum Nivel Ini 4',
			'enum_nivel_fin_3' => 'Enum Nivel Fin 3',
			'enum_nivel_fin_4' => 'Enum Nivel Fin 4',
			'cod_editor' => 'Cod Editor',
			'cod_editor_inst' => 'Cod Editor Inst',
			'edicion' => 'Edicion',
			'fecha_iso' => 'Fecha Iso',
			'fecha_iso_final' => 'Fecha Iso Final',
			'fecha_pub' => 'Fecha Pub',
			'isbn' => 'Isbn',
			'indice_suplemento' => 'Indice Suplemento',
			'cod_conf' => 'Cod Conf',
			'cod_proy' => 'Cod Proy',
			'disemina' => 'Disemina',
			'url' => 'Url',
			'n_disk_cin' => 'N Disk Cin',
			'sala' => 'Sala',
			'impresion' => 'Impresion',
			'orden' => 'Orden',
			'cerrado' => 'Cerrado',
			'hora_iso_inicio' => 'Hora Iso Inicio',
			'hora_iso_final' => 'Hora Iso Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('acceso',$this->acceso,true);
		$criteria->compare('ext_acceso',$this->ext_acceso,true);
		$criteria->compare('jerarquia',$this->jerarquia,true);
		$criteria->compare('ubicacion',$this->ubicacion,true);
		$criteria->compare('tipo_liter',$this->tipo_liter,true);
		$criteria->compare('nivel_bibl',$this->nivel_bibl,true);
		$criteria->compare('nivel_reg',$this->nivel_reg,true);
		$criteria->compare('tipo_ref_analitica',$this->tipo_ref_analitica,true);
		$criteria->compare('paginas',$this->paginas,true);
		$criteria->compare('volumen',$this->volumen,true);
		$criteria->compare('volumen_final',$this->volumen_final,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('numero_final',$this->numero_final,true);
		$criteria->compare('numeracion',$this->numeracion,true);
		$criteria->compare('enum_nivel_ini_3',$this->enum_nivel_ini_3,true);
		$criteria->compare('enum_nivel_ini_4',$this->enum_nivel_ini_4,true);
		$criteria->compare('enum_nivel_fin_3',$this->enum_nivel_fin_3,true);
		$criteria->compare('enum_nivel_fin_4',$this->enum_nivel_fin_4,true);
		$criteria->compare('cod_editor',$this->cod_editor,true);
		$criteria->compare('cod_editor_inst',$this->cod_editor_inst,true);
		$criteria->compare('edicion',$this->edicion,true);
		$criteria->compare('fecha_iso',$this->fecha_iso,true);
		$criteria->compare('fecha_iso_final',$this->fecha_iso_final,true);
		$criteria->compare('fecha_pub',$this->fecha_pub,true);
		$criteria->compare('isbn',$this->isbn,true);
		$criteria->compare('indice_suplemento',$this->indice_suplemento,true);
		$criteria->compare('cod_conf',$this->cod_conf,true);
		$criteria->compare('cod_proy',$this->cod_proy,true);
		$criteria->compare('disemina',$this->disemina,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('n_disk_cin',$this->n_disk_cin,true);
		$criteria->compare('sala',$this->sala,true);
		$criteria->compare('impresion',$this->impresion,true);
		$criteria->compare('orden',$this->orden,true);
		$criteria->compare('cerrado',$this->cerrado,true);
		$criteria->compare('hora_iso_inicio',$this->hora_iso_inicio,true);
		$criteria->compare('hora_iso_final',$this->hora_iso_final,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
          public  function busquedaExpediente($cedula){
              $resultado='';
            $sql="SELECT * FROM (select * from (select row_number() OVER(order by c.numero) as Nro, descrip_desc as Etiqueta, descriptor_salida as Valor from coddesc c,descript d, tipodesc t, document a
                        where 
                        a.ubicacion=:ubicacion and a.ext_acceso = 0 and a.acceso = c.acceso and
                        c.cod_desc = d.codesc and 
                        t.tipo_desc = d.tipo
                         ) uno

                        union 
                        select * from (select 98 as Nro, 'Fecha Nacimiento' as Etiqueta, a.fecha_iso_final as Valor
                        from document a
                        where 
                        a.ubicacion=:ubicacion and a.ext_acceso = 0 ) dos

                        union 
                        select * from (select 99 as Nro, 'Fecha de Ingreso' as Etiqueta, a.fecha_iso as Valor
                        from document a
                        where 
                        a.ubicacion=:ubicacion and a.ext_acceso = 0 ) tres 

                        union 
                        select * from (select 0 as Nro, 'Cédula de identidad' as Etiqueta, a.ubicacion as Valor
                        from document a
                        where 
                        a.ubicacion=:ubicacion and a.ext_acceso = 0 ) cuatro

                        union 
                         select * from (SELECT 0 as nro, 'Nombres y Apellidos' as Etiqueta, t.titulo_salida as Valor
                         FROM codtit c 
                          inner join titulos t on t.cod_titulo=c.cod_titulo and c.ext_acceso = 0
                          inner join document d on c.acceso=d.acceso where d.ubicacion=:ubicacion and d.ext_acceso = 0) cinco
                          
                          

                        ) total 
                        order by Nro"
                         ;

            $query=Yii::app()->db->createCommand($sql);
            $query->bindParam(':ubicacion', $cedula,  PDO::PARAM_STR);
            $resultado=$query->queryAll();
            //var_dump($resultado); die();
            return $resultado;
            
        }
        public function foto($cedula)
                {
                  $sql="SELECT '<img src=\"http://172.16.3.115' || archivo || '\" /> ' as url
FROM hipertextos h inner join document p on h.acceso = p.acceso  where p.ubicacion=:ubicacion and h.ext_acceso = 0";
                  $consulta=Yii::app()->db->createCommand($sql);
                   $consulta->bindParam(':ubicacion', $cedula,  PDO::PARAM_STR);
                  $resultado=$consulta->queryRow();
                  
                 return $resultado;

        }
        
        public function busquedaHiperTexto($cedula){
 //           $sql = "SELECT '<li><a target=_doc href=\"http://172.16.80.127' || archivo || '\" > ' || descripcion || '</a></li>' as url
 // FROM hipertextos h, document d where h.acceso = d.acceso and d.ext_acceso = 0 and d.ubicacion = '".$cedula."'";
            
                        $sql = "SELECT '<li><a target=_doc href=\"http://172.16.3.115' || archivo || '\" > ' || descripcion || '</a></li>' as url, h.ext_acceso
  FROM hipertextos h, document d where h.acceso = d.acceso and d.ext_acceso = 0 and d.ubicacion = '".$cedula."' and h.ext_acceso <>0 order by h.ext_acceso asc";
                        
                        
            $query=Yii::app()->db->createCommand($sql);
            $query->bindParam(':ubicacion', $cedula,  PDO::PARAM_STR);
            $resultado=$query->queryAll();
            //var_dump($resultado); die();
            return $resultado;
        }
}
