<?php

/**
 * This is the model class for table "gplantel.plantel_pae".
 *
 * The followings are the available columns in table 'gplantel.plantel_pae':
 * @property integer $id
 * @property integer $plantel_id
 * @property string $pae_activo
 * @property string $fecha_inicio
 * @property string $fecha_ultima_actualizacion
 * @property integer $matricula_general
 * @property string $posee_simoncito
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $tipo_servicio_pae_id
 * @property integer $matricula_pequena
 * @property integer $matricula_mediana
 * @property integer $matricula_grande
 * @property string $posee_area_cocina
 * @property integer $condicion_servicio_id
 *
 * The followings are the available model relations:
 * @property CondicionServicio $condicionServicio
 * @property TipoServicioPae $tipoServicioPae
 * @property Plantel $plantel
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class PlantelPae extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.plantel_pae';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pae_activo, tipo_servicio_pae_id', 'required'),
			array('plantel_id, matricula_general, usuario_ini_id, usuario_act_id, tipo_servicio_pae_id, matricula_pequena, matricula_mediana, matricula_grande, condicion_servicio_id', 'numerical', 'integerOnly'=>true),
			array('pae_activo, posee_simoncito, posee_area_cocina', 'length', 'max'=>2),
			array('estatus', 'length', 'max'=>1),
			array('fecha_inicio, fecha_ultima_actualizacion, fecha_ini, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, plantel_id, pae_activo, fecha_inicio, fecha_ultima_actualizacion, matricula_general, posee_simoncito, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, tipo_servicio_pae_id, matricula_pequena, matricula_mediana, matricula_grande, posee_area_cocina, condicion_servicio_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'condicionServicio' => array(self::BELONGS_TO, 'CondicionServicio', 'condicion_servicio_id'),
			'tipoServicioPae' => array(self::BELONGS_TO, 'TipoServicioPae', 'tipo_servicio_pae_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'plantel_id' => 'Plantel',
			'pae_activo' => 'Pae Activo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_ultima_actualizacion' => 'Fecha Ultima Actualizacion',
			'matricula_general' => 'Matricula General',
			'posee_simoncito' => 'Posee Simoncito',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'tipo_servicio_pae_id' => 'Tipo Servicio Pae',
			'matricula_pequena' => 'Matricula Pequena',
			'matricula_mediana' => 'Matricula Mediana',
			'matricula_grande' => 'Matricula Grande',
			'posee_area_cocina' => 'Posee Area Cocina',
			'condicion_servicio_id' => 'Condicion Servicio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('pae_activo',$this->pae_activo,true);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_ultima_actualizacion',$this->fecha_ultima_actualizacion,true);
		$criteria->compare('matricula_general',$this->matricula_general);
		$criteria->compare('posee_simoncito',$this->posee_simoncito,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('tipo_servicio_pae_id',$this->tipo_servicio_pae_id);
		$criteria->compare('matricula_pequena',$this->matricula_pequena);
		$criteria->compare('matricula_mediana',$this->matricula_mediana);
		$criteria->compare('matricula_grande',$this->matricula_grande);
		$criteria->compare('posee_area_cocina',$this->posee_area_cocina,true);
		$criteria->compare('condicion_servicio_id',$this->condicion_servicio_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function activarPae($plantel_id, $posee_simoncito, $tipo_servicio_pae, $posee_area_cocina, $condicion_servicio_id){
            $usuario_id = Yii::app()->user->id;
            $estatus = 'A';
            $fecha_ini = date('Y-m-d H:i:s');
        
            $sql = "INSERT INTO gplantel.plantel_pae(
                        plantel_id, pae_activo, fecha_inicio, fecha_ultima_actualizacion, posee_simoncito, usuario_ini_id, fecha_ini,
                        estatus, tipo_servicio_pae_id, posee_area_cocina, condicion_servicio_id)
                VALUES ($plantel_id, 'SI', '$fecha_ini', '$fecha_ini', '$posee_simoncito', $usuario_id, '$fecha_ini', '$estatus', $tipo_servicio_pae, 
                        '$posee_area_cocina', $condicion_servicio_id) RETURNING *;";
            $resultado = Yii::app()->db->createCommand($sql);
            return $resultado->queryAll();
        }

        
         public function datosPlantelOrdenCompra($plantel_id) {
        if (is_numeric($plantel_id)) {
            $sql = "SELECT p.id as plantel_id,
                           z.id as zona_id,
                           p.nombre as plantel_nombre,
                           z.nombre as zona_nombre,
                           pv.razon_social as proveedor_nombre,
                           pap.pae_activo,
                           oc.id as orden_compra,
                           e.nombre as estado,
                           m.nombre as municipio,
                           pap.tipo_servicio_pae_id as tipo_servicio
                    FROM gplantel.plantel p
                    LEFT JOIN gplantel.zona_educativa z ON z.id = p.zona_educativa_id
                    LEFT JOIN gplantel.plantel_proveedor pp ON pp.plantel_id = p.id
                    LEFT JOIN proveduria.proveedor pv ON pv.id = pp.proveedor_id
                    LEFT JOIN gplantel.plantel_pae pap ON pap.plantel_id = p.id
                    LEFT JOIN public.estado e ON e.id = p.estado_id
                    LEFT JOIN public.municipio m ON m.id = p.municipio_id
                    LEFT JOIN administrativo.orden_compra oc ON oc.dependencia = p.id AND (EXTRACT (MONTH FROM oc.fecha_ini)) = (EXTRACT (MONTH FROM NOW())) 
                    WHERE p.id = :plantel_id";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
            return $resultado;
        } else {
            return null;
        }
    }
        
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PlantelPae the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
