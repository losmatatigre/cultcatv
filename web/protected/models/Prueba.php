<?php

/**
 * This is the model class for table "sistema.prueba".
 *
 * The followings are the available columns in table 'sistema.prueba':
 * @property integer $id
 * @property string $nombre
 * @property string $url
 * @property string $extencion
 * @property integer $usuario_ini
 * @property integer $usuario_act
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class Prueba extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sistema.prueba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario_ini, usuario_act', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>40),
			array('url', 'length', 'max'=>100),
			array('extencion', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, url, extencion, usuario_ini, usuario_act', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'url' => 'Url',
			'extencion' => 'Extencion',
			'usuario_ini' => 'Usuario Ini',
			'usuario_act' => 'Usuario Act',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('extencion',$this->extencion,true);
		$criteria->compare('usuario_ini',$this->usuario_ini);
		$criteria->compare('usuario_act',$this->usuario_act);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prueba the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
