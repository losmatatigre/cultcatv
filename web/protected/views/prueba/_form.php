<?php
/* @var $this PruebaController */
/* @var $model Prueba */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prueba-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'extencion'); ?>
		<?php echo $form->textField($model,'extencion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'extencion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_ini'); ?>
		<?php echo $form->textField($model,'usuario_ini'); ?>
		<?php echo $form->error($model,'usuario_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_act'); ?>
		<?php echo $form->textField($model,'usuario_act'); ?>
		<?php echo $form->error($model,'usuario_act'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->