<?php
/* @var $this FicheroController */
/* @var $model Fichero */

$this->breadcrumbs=array(
	'Ficheroes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Fichero', 'url'=>array('index')),
	array('label'=>'Create Fichero', 'url'=>array('create')),
	array('label'=>'View Fichero', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Fichero', 'url'=>array('admin')),
);
?>

<h1>Update Fichero <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>