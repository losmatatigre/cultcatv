<?php
/* @var $this FicheroController */
/* @var $model Fichero */

$this->breadcrumbs=array(
	'Ficheroes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Fichero', 'url'=>array('index')),
	array('label'=>'Manage Fichero', 'url'=>array('admin')),
);
?>

<h1>Create Fichero</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>