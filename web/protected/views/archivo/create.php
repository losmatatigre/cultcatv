<?php
/* @var $this ArchivoController */
/* @var $model Archivo */

$this->pageTitle = 'Registro de Archivos';

      $this->breadcrumbs=array(
	'Archivos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>