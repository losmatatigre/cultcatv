<?php
/* @var $this ArchivoController */
/* @var $model Archivo */

$this->pageTitle = 'Actualización de Datos de Archivos';

      $this->breadcrumbs=array(
	'Archivos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>