<?php
$this->breadcrumbs=array(
	'Trazas'=>array('index'),
	$model->id_traza,
);

$this->menu=array(
	array('label'=>'List Traza', 'url'=>array('index')),
	//array('label'=>'Create Traza', 'url'=>array('create')),
	//array('label'=>'Update Traza', 'url'=>array('update', 'id'=>$model->id_traza)),
	//array('label'=>'Delete Traza', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_traza),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Traza', 'url'=>array('admin')),
);
?>

<h1>Ver Traza #<?php echo $model->id_traza; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_traza',
		'username',
		'fecha_hora',
		'ip_maquina',
		'tipo_transaccion',
		'modulo',
	),
)); ?>
