<?php
$this->breadcrumbs=array(
	'Trazas'=>array('index'),
	'Buscar',
);

//$this->menu=array(
//	array('label'=>'List Traza', 'url'=>array('index')),
	//array('label'=>'Create Traza', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('traza-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Busqueda de Traza</h1>

<p>
Si lo desea, puede introducir un operador de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada uno de los valores de su búsqueda para especificar cómo la comparación se debe hacer.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'traza-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_traza',
		'username',
		'fecha_hora',
		'ip_maquina',
		'tipo_transaccion',
		'modulo',
		array(
			'class'=>'CButtonColumn',
			'deleteButtonOptions'=>array('style'=>'display:none'),
			'updateButtonOptions'=>array('style'=>'display:none'),
		),
	),
)); ?>
