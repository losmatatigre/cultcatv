<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->breadcrumbs=array(
	'Documents'=>array('lista'),
);
?>

<div class="tabbable">

    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <div class="form">

        <div id="div-datos-generales">

            <div class="widget-box">

                <div class="widget-header">
                    <h5>Datos Generales</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'acceso'); ?>
                                            <?php echo $form->textField($model,'acceso',array('size'=>13, 'maxlength'=>13, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'ext_acceso'); ?>
                                            <?php echo $form->textField($model,'ext_acceso',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'jerarquia'); ?>
                                            <?php echo $form->textField($model,'jerarquia',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'ubicacion'); ?>
                                            <?php echo $form->textField($model,'ubicacion',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'tipo_liter'); ?>
                                            <?php echo $form->textField($model,'tipo_liter',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'nivel_bibl'); ?>
                                            <?php echo $form->textField($model,'nivel_bibl',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'nivel_reg'); ?>
                                            <?php echo $form->textField($model,'nivel_reg',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'tipo_ref_analitica'); ?>
                                            <?php echo $form->textField($model,'tipo_ref_analitica',array('size'=>2, 'maxlength'=>2, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'paginas'); ?>
                                            <?php echo $form->textField($model,'paginas',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'volumen'); ?>
                                            <?php echo $form->textField($model,'volumen',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'volumen_final'); ?>
                                            <?php echo $form->textField($model,'volumen_final',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'numero'); ?>
                                            <?php echo $form->textField($model,'numero',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'numero_final'); ?>
                                            <?php echo $form->textField($model,'numero_final',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'numeracion'); ?>
                                            <?php echo $form->textField($model,'numeracion',array('size'=>60, 'maxlength'=>64, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'enum_nivel_ini_3'); ?>
                                            <?php echo $form->textField($model,'enum_nivel_ini_3',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'enum_nivel_ini_4'); ?>
                                            <?php echo $form->textField($model,'enum_nivel_ini_4',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'enum_nivel_fin_3'); ?>
                                            <?php echo $form->textField($model,'enum_nivel_fin_3',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'enum_nivel_fin_4'); ?>
                                            <?php echo $form->textField($model,'enum_nivel_fin_4',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_editor'); ?>
                                            <?php echo $form->textField($model,'cod_editor',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_editor_inst'); ?>
                                            <?php echo $form->textField($model,'cod_editor_inst',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'edicion'); ?>
                                            <?php echo $form->textField($model,'edicion',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_iso'); ?>
                                            <?php echo $form->textField($model,'fecha_iso',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_iso_final'); ?>
                                            <?php echo $form->textField($model,'fecha_iso_final',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_pub'); ?>
                                            <?php echo $form->textField($model,'fecha_pub',array('size'=>60, 'maxlength'=>80, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'isbn'); ?>
                                            <?php echo $form->textField($model,'isbn',array('size'=>20, 'maxlength'=>20, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'indice_suplemento'); ?>
                                            <?php echo $form->textField($model,'indice_suplemento',array('size'=>32, 'maxlength'=>32, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_conf'); ?>
                                            <?php echo $form->textField($model,'cod_conf',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cod_proy'); ?>
                                            <?php echo $form->textField($model,'cod_proy',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'disemina'); ?>
                                            <?php echo $form->textField($model,'disemina',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'url'); ?>
                                            <?php echo $form->textField($model,'url',array('size'=>60, 'maxlength'=>255, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'n_disk_cin'); ?>
                                            <?php echo $form->textField($model,'n_disk_cin',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'sala'); ?>
                                            <?php echo $form->textField($model,'sala',array('size'=>35, 'maxlength'=>35, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'impresion'); ?>
                                            <?php echo $form->textField($model,'impresion',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'orden'); ?>
                                            <?php echo $form->textField($model,'orden',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cerrado'); ?>
                                            <?php echo $form->textField($model,'cerrado',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'hora_iso_inicio'); ?>
                                            <?php echo $form->textField($model,'hora_iso_inicio',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'hora_iso_final'); ?>
                                            <?php echo $form->textField($model,'hora_iso_final',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div><!-- form -->
        </div>
    </div>
</div>
