<?php
$this->breadcrumbs = array(
    'Mi Perfil',
);

$this->pageTitle = 'Mi Perfil';
?>
<div class="col-xs-12">


    <div class="nav-tabs-custom">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#perfil">Mis Datos de Acceso</a>
            </li>
            <li>
                <a data-toggle="tab" href="#contacto">Mis Datos de Contacto</a>
            </li>
        </ul>

        <div class="tab-content">

            <div id="perfil"  class="tab-pane active">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-groups-password-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ));
                ?>
                <br><br>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Mis Datos de Acceso</h3>
                        <div class="box-tools pull-right">
                            <a data-widget="collapse" class="btn btn-primary btn-sm"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="box-body">


                        <div class="row col-md-12">
                            <div class="row-fluid" id="resultado">
                                <?php if ($form->errorSummary($passModel) !== ''): ?>
                                    <div class="errorDialogBox"><?php echo $form->errorSummary($passModel); ?></div>
                                <?php else: ?>
                                    <div class="infoDialogBox">
                                        <p>
                                            Todos los campos con <code>*</code> son requeridos.
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>



                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="groupname">Cédula</label>
                                    <input class="form-control" type="text" style="width: 80%;" disabled value="<?php echo Yii::app()->user->cedula; ?>" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description">Nombre</label>
                                    <input class="form-control"  type="text" style="width: 80%;" disabled value="<?php echo Yii::app()->user->nombre; ?>" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description">Apellido</label>
                                    <input class="form-control" type="text" style="width: 80%;" disabled value="<?php echo Yii::app()->user->apellido; ?>" />
                                </div>
                            </div>






                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="groupname">Estado</label>
                                    <input class="form-control" type="text" style="width: 80%;" disabled value="<?php echo Yii::app()->user->estadoName; ?>" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description">Usuario</label>
                                    <input class="form-control" type="text" style="width: 80%;" disabled value="<?php echo Yii::app()->user->name; ?>" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description">Último Login</label>
                                    <input class="form-control" type="text" style="width: 80%;" disabled value="<?php echo Utiles::transformDate(substr(Yii::app()->user->lastLoginTime, 0, 10), '-', 'ymd', 'dmy') . substr(Yii::app()->user->lastLoginTime, 10, 9); ?>" />
                                </div>
                            </div>




                            <?php echo $form->hiddenField($passModel, 'old_password', array('value' => 'filler')) ?>



                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="groupname" class="col-md-12">Clave Actual <code>*</code></label>
                                    <?php echo CHtml::passwordField('UserGroupsUser[old_password]', '', array('class' => 'form-control', 'id' => 'UserGroupsUser_old_password', 'size' => 60, 'maxlength' => 120, 'style' => 'width: 80%;', 'required' => 'required')); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description" class="col-md-12">Nueva Clave <code>*</code></label>
                                    <?php echo $form->passwordField($passModel, 'password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 120, 'style' => 'width: 80%;', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description" class="col-md-12">Confirme la Nueva Clave <code>*</code></label>
                                    <?php echo $form->passwordField($passModel, 'password_confirm', array('class' => 'form-control', 'size' => 60, 'maxlength' => 120, 'style' => 'width: 80%;', 'required' => 'required')); ?>
                                </div>
                            </div>





                            <input type="hidden" name="changePasswordToken" value="<?php echo $token; ?>" />

                            <?php echo CHtml::hiddenField('formID', $form->id) ?>

                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="row col-md-12">
                            <div class="col-xs-6">
                                <a href="/site" class="btn btn-danger">
                                    <i class="icon-arrow-left"></i>
                                    Volver
                                </a>
                            </div>

                            <div class="col-xs-6">
                                <button class="btn btn-primary btn-next pull-right" title="Cambiar Clave de Acceso" data-last="Finish" type="submit">
                                    Cambiar Clave
                                    <i class="icon-exchange icon-on-right"></i>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>


                <div id="dialog-passwd" class="hide">
                    <div class="alertDialogBox">
                        <p id="mensaje-confirm">
                            ¿Confirma el cambio de clave?
                        </p>
                    </div>
                </div>

            </div>

            <div id="contacto" class="tab-pane">



                <form name="user-groups-contact-form" id="user-groups-contact-form" method="POST" >
                    <br><br>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Mis Datos de Contacto</h3>
                            <div class="box-tools pull-right">
                                <a data-widget="collapse" class="btn btn-primary btn-sm"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row col-md-12">
                                <div class="row-fluid" id="resultado-contacto">
                                    <?php if ($form->errorSummary($passModel) !== ''): ?>
                                        <div class="errorDialogBox"><?php echo $form->errorSummary($passModel); ?></div>
                                    <?php else: ?>
                                        <div class="infoDialogBox">
                                            <p>
                                                Todos los campos con <code>*</code> son requeridos.
                                            </p>
                                        </div>
                                    <?php endif; ?>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="description" >Nombre</label>
                                        <input class="form-control" type="text" style="width: 90%;" disabled value="<?php echo Yii::app()->user->nombre; ?>" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="description" >Apellido</label>
                                        <input class="form-control" type="text" style="width: 90%;" disabled value="<?php echo Yii::app()->user->apellido; ?>" />
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="groupname" >Teléfono Fijo <code>*</code></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-phone"></i></span>
                                            <?php echo $form->textField($passModel, 'telefono', array('class' => 'form-control', 'size' => 60, 'maxlength' => 11, 'style' => 'width: 90%;', 'required' => 'required', "autocomplete" => "off", 'placeholder' => 'Teléfono Fijo con Código de Área',)); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="description" >Teléfono Celular</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa-mobile fa"></i></span>
                                            <?php echo $form->textField($passModel, 'telefono_celular', array('class' => 'form-control', 'size' => 60, 'maxlength' => 11, 'style' => 'width: 90%;', "autocomplete" => "off", 'placeholder' => 'Teléfono Celular con Código de Operadora',)); ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description" >Correo Electrónico <code>*</code></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            <?php echo $form->emailField($passModel, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 120, 'style' => 'width: 90%;', 'required' => 'required', "autocomplete" => "off", 'placeholder' => 'Correo Electrónico',)); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="description">Twitter</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa-twitter fa"></i></span>
                                            <?php echo $form->textField($passModel, 'twitter', array('size' => 60, 'maxlength' => 40, 'style' => 'width: 90%;', 'class' => 'form-control', "autocomplete" => "off", 'placeholder' => 'Nombre de Usuario en Twitter',)); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <input type="hidden" name="changePasswordToken" value="<?php echo $token; ?>" />

                            <input type="hidden" id="formID" name="formID" value="user-groups-contact-form">
                        </div>

                        <div class="box-footer">
                            <br>
                            <div class="row col-md-12">

                                <div class="col-xs-6">
                                    <a href="/site" class="btn btn-danger">
                                        <i class="icon-arrow-left"></i>
                                        Volver
                                    </a>
                                </div>

                                <div class="col-xs-6">
                                    <button class="btn btn-primary btn-next pull-right" title="Guardar Datos de Contacto" data-last="Finish" type="submit">
                                        Guardar Datos
                                        <i class="icon-save icon-on-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>

        </div>
    </div>
</div>



</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/userGroups/usuario/perfil.min.js', CClientScript::POS_END); ?>