<?php

//Se encarga de capturar los items a pintar en el Menu
function getMenu() {

    $items = array();
    $sub_items = array();

    //Inicio  
    $items[] = array('name' => 'Inicio', 'code' => 'inicio', 'icon' => 'fa fa-home', 'link' => array('/site'));
    
    //Videos
    if (Yii::app()->user->pbac('videos.consultar.read') || (Yii::app()->user->pbac("videos.consultar.write"))) {
        $items[] = array('name' => 'Videos', 'code' => 'videos', 'icon' => 'fa fa-video-camera', 'link' => array('/documentos/fichero'));
    }
    //Programacion
    if (Yii::app()->user->pbac('programacion.consultar.read') || (Yii::app()->user->pbac("programacion.consultar.write"))) {
        $items[] = array('name' => 'Programación', 'code' => 'videos', 'icon' => 'fa fa-list-ul', 'link' => array('/programacion/consultar/'));
    }
    //Seguridad
    if (Yii::app()->user->pbac('Basic.traza.read') || (Yii::app()->user->pbac("userGroups.admin.admin"))) {
        $items[] = array('name' => 'Seguridad', 'code' => 'seguridad', 'icon' => 'fa fa-lock', 'sub' => getSubMenu('Seguridad'));
    }

    //Cerrar Sesión
    $items[] = array('name' => '&nbsp;&nbsp;Cerrar Sesión', 'code' => 'cerrar-sesion', 'icon' => 'icon-off', 'link' => array('/logout'));



    return $items;
}

// Se encarga de capturar los items a pintar en el SubMenu del Menu
function getSubMenu($menu) {
    $items = array();

    switch ($menu) {

        case 'Seguridad':
            if (Yii::app()->user->pbac("userGroups.admin.admin") || Yii::app()->user->pbac("userGroups.admin.write")) {
                $items[] = array('name' => 'Grupos', 'code' => 'usuarios', 'link' => array('/userGroups/grupo/'));
                $items[] = array('name' => 'Usuario', 'code' => 'usuarios', 'link' => array('/userGroups/usuario/'));
                $items[] = array('name' => 'Administracion', 'code' => 'admin', 'link' => array('/userGroups/admin/'));
            }

            if (Yii::app()->user->pbac('Basic.traza.read')) {
                $items[] = array('name' => 'Buscar Traza', 'code' => 'buscar-traza', 'link' => array('/traza/admin'));

                $items[] = array('name' => 'Ver Trazas', 'code' => 'ver-traza', 'link' => array('/traza/index'));
            }
            break;
    }

    return $items;
}

$_SESSION['_items_menu'] = getMenu();
//Defino mi lista de items a mostrar (menus y submenus) si y solo si ya no lo tengo en session
if (!isset($_SESSION['_items_menu'])) {
    $_SESSION['_items_menu'] = getMenu();
}

//Pinto el menu
//	$this->widget('application.extensions.mbmenu.MbMenu',array('items'=>$_SESSION['_items_menu']));

$this->widget('application.widgets.EMenuWidget', array('items' => $_SESSION['_items_menu']));
