<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<section class="content">
                 
                    <div class="error-page">
                        <h2 class="headline text-info"> <?php echo $code; ?></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i>  <?php echo CHtml::encode($message); ?> </h3>
                            <p>
                                 
                                Puedes Regresar al inicio siguiendo este enlace <a href="/site">Volver al Inicio!</a> 
                            </p>
                        </div><!-- /.error-content -->
                    </div><!-- /.error-page -->

                </section>
