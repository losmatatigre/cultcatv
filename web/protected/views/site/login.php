
<body class="login-layout">	

<div class="form-box" id="login-box">
            <div class="header">Sign In</div>
            <!--<form action="../../index.html" method="post">-->
               <?php
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'login-form',
                                            'enableClientValidation' => true,
                                            'clientOptions' => array(
                                                'validateOnSubmit' => true,
                                            ),
                                        ));
                                        ?>
                <div class="body bg-gray">
                    <div class="form-group">
                        <?php echo $form->textField($model, 'usuario', array('class' => 'form-control', 'placeholder' => "Usuario de Red", 'required' => 'required')); ?>
                        
                    </div>
                    <div class="form-group">
                        <?php echo $form->passwordField($model, 'clave', array('class' => 'form-control', 'placeholder' => "Contraseña", 'required' => 'required')); ?>

                    </div>          
                    <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>  
                    
                    <p><a href="#">I forgot my password</a></p>
                    
                    <a href="register.html" class="text-center">Register a new membership</a>
                </div>

                 <?php $this->endWidget(); ?>
            <!--</form>-->

            <div class="margin text-center">
                <span>Sign in using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

            </div>
        </div>




                
    <script type="text/javascript">
        function show_box(id) {
            jQuery('.widget-box.visible').removeClass('visible');
            jQuery('#' + id).addClass('visible');
        }
    </script>
</body>
