<div class="col-sm-10 col-sm-offset-1">
    <div class="login-container">

        <div class="space-6"></div>

        <div class="position-relative">

            <div id="login-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">
                            <i class="fa fa-globe green"></i>
                            Navegador Web No Permitido
                        </h4>


                        <p class="header gray lighter bigger"> Estimado usuario, esta aplicación no puede ser ejecutada en este navegador. Le recomendamos los siguientes navegadores.</p>
                        <div class="space-6"></div>
                        <p  align="center">
                            <a href="http://www.mozilla.org/es-ES/firefox/new/"><img height="60" alt="Mozilla Firefox" <?php echo 'src="' . Yii::app()->baseUrl . '/public/images/firefox.png' . '"' ?>/></a>
                            <a href="http://www.google.com/intl/es-419/chrome/browser/"><img height="60"alt="Google Chrome"  <?php echo 'src="' . Yii::app()->baseUrl . '/public/images/chrome.png' . '"' ?>/></a>
                            <a href="https://download-chromium.appspot.com/"><img height="60"alt="Chromium"  <?php echo 'src="' . Yii::app()->baseUrl . '/public/images/chromium.png' . '"' ?>/></a>
                        </p>

                    </div><!-- /widget-main -->

                </div><!-- /widget-body -->
            </div><!-- /login-box -->


        </div><!-- /position-relative -->
    </div>
</div><!-- /.col -->