<?php
/* @var $this FicheroController */
/* @var $model Fichero */

$this->breadcrumbs=array(
	'Ficheroes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Fichero', 'url'=>array('index')),
	array('label'=>'Create Fichero', 'url'=>array('create')),
	array('label'=>'Update Fichero', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Fichero', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fichero', 'url'=>array('admin')),
);
?>

<h1>View Fichero #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'fecha',
		'url',
		'extencion',
		'usuario_ini',
		'usuario_act',
	),
)); ?>
