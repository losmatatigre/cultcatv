<?php
/* @var $this FicheroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ficheroes',
);

$this->menu=array(
	array('label'=>'Create Fichero', 'url'=>array('create')),
	array('label'=>'Manage Fichero', 'url'=>array('admin')),
);
?>

<h1>Ficheroes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
