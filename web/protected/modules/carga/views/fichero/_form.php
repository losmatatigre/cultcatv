<?php
/* @var $this ClasePlantelController */
/* @var $model ClasePlantel */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => '',
    'enableAjaxValidation' => false,
        ));
?>
<table>
<div class="widget-body">

    <div class="widget-main form">


        <?php
        if ($form->errorSummary($model)):
            ?>
            <div id ="div-result-message" class="errorDialogBox" >

                <?php echo $form->errorSummary($model); ?>
            </div>
            <?php
        endif;
        ?>
        <tr>
            <td>
      
            <div class="col-md-6">
                <label class="col-md-12" for="nombre">Nombre o Descripción</label>
                <?php
                echo
                $form->textField($model, 'nombre', array('size' => 40, 'maxlength' => 50, 'class
' => 'col-xs-30 col-sm-30', 'required' => 'required'));
                ?>
            </div> 
                 </td>
                  </tr>
   
                 
                  <tr>  
                      <td>
</div>

    <div> 
         <br>
  <?php $this->renderPartial('_archivo', array('model' => $model)); ?>
        </div>
   </div>
         </div>
         </td>
             
         </tr>

         <tr>
             <td>
  <div>
                        <a class="btn btn-danger" href="/documentos/fichero">

                            <i class="icon-arrow-left bigger-110"></i>
                            Volver
                        </a>

                        <button class="btn btn-primary btn-next" data-last="Finish">
                            Guardar
                            <i class=" icon-save"></i>
                        </button>
                    </div>
                 </td>
         </tr>
</table>


<?php $this->endWidget(); ?>



    <script>
        $(document).ready(function() {

            $("#nombre").keyup(function() {
                //makeUpper(this);
                makeUpper(this);

            });

            $('#nombre').bind('keyup blur', function() {
                keyText(this, true);

            });
            $('#nombre').bind('blur', function() {
                clearField(this);
            });

        });

    </script>

    <!-- form -->


