
<div class="col-lg-12">
    <span class="btn btn-success smaller-90 fileinput-button">
        <i class="fa fa-upload"></i>
        <span> Cargue el archivo...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" >
    </span>
    <div id="notificacionArchivo" ></div>
    <input id="nombreArchivo" type="hidden" name="nombreArchivo" >
    <input id="nombreBD" type="hidden" readonly="readonly" name="nombreBD" >
<!-- The container for the uploaded files -->
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>


<div id="dialogPantalla" class="hide"></div>
<div id="files" class="files"></div>

<div id="listado">
</div>
<div id="subDialogPantalla"></div>
<br>
</div>
<!-- The template to display files available for upload -->

<!-- The main application script -->
<script>
    $(document).ready(function() {
        var id = $("#id").val();
        $('#fileupload').fileupload({
            url: '/documentos/fichero/upload?id=' + id,
          acceptFileTypes: /(\.|\/)(jpe?g|png|pdf|doc|opt|avi|bik|div|mov|mp4|mp3|mpa|mpe|mpg|rad|rpm|smk|vm|wmv|wob)$/i,
            maxFileSize: 5000000,
            singleFileUploads: true,
            autoUpload: true,
            process: [
                {
                    action: 'load',
                    fileTypes: /(\.|\/)(jpe?g|png|pdf|doc|opt|avi|bik|div|mov|mp4|mp3|mpa|mpe|mpg|rad|rpm|smk|vm|wmv|wob)$/i,
                    maxFileSize: 20000000 // 20MB
                },
                {
                    action: 'resize',
                    maxWidth: 1440,
                    maxHeight: 900
                },
                {
                    action: 'save'
                }
            ],
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Se ha producido un error en la carga del archivo.");//ALEXIS ERROR DE CARGA
            }

        });
        $('#fileupload').bind('fileuploaddone', function(e, data) {
            var archivos = data.jqXHR.responseJSON.files;

            $("#notificacionArchivo").html("¡Archivo cargado con exito!");

            $.each(archivos, function(index, file) {
                var nombre = file.name;
                $("#nombreBD").val(nombre);
                $("#nombreArchivo").val(nombre);
            });
        });
    });
    function getFileExtension(name)
    {
        var found = name.lastIndexOf('.') + 1;
        return (found > 0 ? name.substr(found) : "");
    }
</script>

<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload-ui.css">
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/public/js/jquery.upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/public/js/jquery.upload/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/public/js/jquery.upload/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="/public/js/jquery.upload/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/js/jquery.upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-ui.js"></script>