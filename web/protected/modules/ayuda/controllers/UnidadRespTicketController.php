<?php

class UnidadRespTicketController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Unidad Responsable de Ticket',
        'write' => 'Gestion de Unidad Responsable de Ticket',
        'label' => 'Módulo de Unidad Responsable de Ticket',
    );

    /**
     * @return array action filters
      public function filters() {
      return array(
      'userGroupsAccessControl', // perform access control for CRUD operations
      'postOnly + delete', // we only allow deletion via POST request
      );
      }
      /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'admin', 'create', 'activar', 'view','updateUnidad'),
                'pbac' => array('read', 'write',),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('update'),
                'pbac' => array('admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = base64_decode($id);
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new UnidadRespTicket;
        $mensaje = '';
        if (isset($_POST['UnidadRespTicket'])) {
            $model->nombre = $_POST['UnidadRespTicket']['nombre'];
            $model->correo_unidad = $_POST['UnidadRespTicket']['correo_unidad'];
            $model->telefono_unidad = $_POST['UnidadRespTicket']['telefono_unidad'];
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = 'A';
            if ($model->validate()) {
                if ($model->save()) {
                    $mensaje = 1;
                    $unidad_id = $model->getPrimaryKey();
                    Yii::app()->getSession()->add('unidad', $model->nombre);
                    Yii::app()->getSession()->add('id', $model->id);
                    $model = $this->loadModel($unidad_id);
                    $id = base64_decode($unidad_id);
                    $this->registerLog('ESCRITURA', 'ayuda.UnidadRespTicket.create', 'EXITOSO', 'Se ha creado un responsable de ticket');
                    $id = base64_encode($model->id);
                    //$model = new UnidadRespTicket;
                    $this->redirect('/ayuda/unidadRespTicket/update/id/' . $id . '', array(
                        'model' => $model, 'mensaje' => $mensaje));
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }
        $this->render('_form', array(
            'model' => $model, 'mensaje' => $mensaje,
        ));
    }
    public function actionUpdate($id) {
        $mensaje = 1;
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
        if (isset($_POST['UnidadRespTicket'])) {
            $model->attributes = $_POST['UnidadRespTicket'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if ($model->validate()) {
                if ($model->save()) {
                    $mensaje = 2;
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }
        $this->render('_form', array(
            'model' => $model, 'mensaje' => $mensaje,
        ));
    }

     public function actionUpdateUnidad($id) {
        $mensaje = 1;
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
        if (isset($_POST['UnidadRespTicket'])) {
            $model->attributes = $_POST['UnidadRespTicket'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if ($model->validate()) {
                if ($model->save()) {
                    $mensaje = 2;
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }
        $this->renderPartial('editar', array(
            'model' => $model, 'mensaje' => $mensaje,
        ));
    }

    public function actionActivar() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->estatus = "A";
                if ($model->save()) {
                    $this->registerLog('ESCRITURA', 'catalogo.UnidadRespTicket.activar', 'EXITOSO', 'Se ha activado una unidad responsable de ticket');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->estatus = "E";
                if ($model->save()) {
                    $this->registerLog('ESCRITURA', 'ayuda.UnidadRespTicket.borrar', 'EXITOSO', 'Se ha eliminado una unidad responsable de ticket');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
        //$this->render('borrar',array('model'=>$model,));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if(!isset($_GET['ajax']))
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $groupId = Yii::app()->user->group;
        $model = new UnidadRespTicket('search');
        if (isset($_GET['UnidadRespTicket']))
            $model->attributes = $_GET['UnidadRespTicket'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('UnidadRespTicket');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    public function columnaAcciones($datas) {
        $id = $datas["id"];
        $id = base64_encode($id);
        $estatus = $datas['estatus'];
        if ($estatus == 'E') {
            $columna = CHtml::link("", "", array("class" => "fa fa-check", "title" => "Activar Istructivo", "onClick" => "VentanaDialog('$id','/ayuda/unidadRespTicket/activar','activar Responsable De Ticket','activar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "title" => "Consultar Datos de la Unidad Responsable de Atención", "onClick" => "VentanaDialog('$id','/ayuda/unidadRespTicket/view','Datos de Responsable de Atención de Solicitudes','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "title" => "Modificar Datos de Unidad Responsable de Atención", "href" => "/ayuda/unidadRespTicket/update/id/$id")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar Datos de Unidad responsable de Atención de Solicitudes", "onClick" => "VentanaDialog('$id','/ayuda/unidadRespTicket/delete','Eliminar Unidad Responsable de Atención Solicitud','borrar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

    public function columnaObservacion($registro) {
        $resultado = '';
        $observacion = $registro['descripcion'];
        var_dump("algo" . $observacion);
        die();
        $resultado = substr($observacion, 0, 30) . '...';
        return $resultado;
    }

    public function estatus($data) {
        $estatus = '';
        $id = $data["estatus"];
        if ($id == 'A') {
            $estatus = 'Activo';
        }
        if ($id == 'E') {
            $estatus = 'Eliminado';
        }
        return $estatus;
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return UnidadRespTicket the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = UnidadRespTicket::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param UnidadRespTicket $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'unidad-resp-ticket-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
