<?php
$this->breadcrumbs = array(
    'Planteles' => '/planteles/',
    'Planificación',
);
?>

<?php
if (isset($plantelPK)) {
    ?>
    <div class = "widget-box collapsed">

        <div class = "widget-header">
            <h5>Identificación del Plantel <?php echo '"' . $plantelPK['nombre'] . '"'; ?></h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid center">
                        <div id="1eraFila" class="col-md-12">
                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if ($plantelPK['cod_plantel'] != null) {
                                    echo CHtml::textField('', $plantelPK['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                } else {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
    <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
    <?php
    if ($plantelPK['cod_estadistico'] != null) {
        echo CHtml::textField('', $plantelPK['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
    } else {
        echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
    }
    ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if ($plantelPK['denominacion_id'] != null) {
                                    echo CHtml::textField('', $plantelPK->denominacion->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                } else {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>

                        <div id="2daFila" class="col-md-12">
                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
    <?php
    if ($plantelPK['nombre'] != null) {
        echo CHtml::textField('', $plantelPK['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
    } else {
        echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
    }
    ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if ($plantelPK['zona_educativa_id'] != null) {
                                    echo CHtml::textField('', $plantelPK->zonaEducativa->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                } else {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if ($plantelPK['estado_id'] != null) {
                                    echo CHtml::textField('', $plantelPK->estado->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                } else {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php
}
?>

<div class = "widget-box">

    <div class = "widget-header">
        <h5>Planificación</h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid center">

                    <div class="main-container" id="main-container">

                        <div class="main-container-inner">

                            <div class="page-content">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                $selectable = 'true';
                                                if($switch == 'bloqueado'){
                                                    $selectable = 'false';
                                                    ?>
                                                <div class="errorDialogBox" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    Lo sentimos, esta temporalmente inhabilitado las planificaciones, por los días vacacionales.
                                                    <br><br></div>
                                                    <br>
                                                    <?php
                                                }
                                                ?>
                                                    <br>
                                                    <div class="space"></div>
                                                    <div id="calendar"></div>
                                            </div>
                                        </div>
                                        <!-- PAGE CONTENT ENDS -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.page-content -->
                        </div><!-- /.main-container-inner -->
                    </div><!-- /.main-container -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {

        /* initialize the external events
         -----------------------------------------------------------------*/

        $('#external-events div.external-event').each(function() {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });

        /* initialize the calendar
         -----------------------------------------------------------------*/

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        var calendar = $('#calendar').fullCalendar({
            disableDragging: true,
            weekends: false,
            buttonText: {
                prev: '<i class="icon-chevron-left" id=""></i>',
                next: '<i class="icon-chevron-right"></i>'
            },
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'month' //agendaWeek,agendaDay
            },
            events: "/planificacion/planificacion/todosEventos/id/" + <?php echo $plantel_id; ?>,
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped
//                var id = this.id;
////                    alert(this.value);
//                var className = this.className.split(' ');
//                var start = $.fullCalendar.formatDate(date, "yyyy-MM-dd HH:mm:ss");
//                var end = $.fullCalendar.formatDate(date, "yyyy-MM-dd HH:mm:ss");
//
//                $.ajax({
//                    url: 'add_events.php',
//                    data: 'id=' + id + '&className=' + className[1] + '&start=' + start + '&end=' + end,
//                    type: "POST",
//                    success: function(json) {
////                            alert('Creado con Éxito.');
//                    }
//                });
//
//                calendar.fullCalendar('renderEvent',
//                {
//                    start: start,
//                    end: end,
//                    allDay: allDay,
//                    className: className[1],
//                },
//                true // make the event "stick"
//                );
//                calendar.fullCalendar('unselect');
                $("#dialogPantallaConsultar").dialog("close");

            }
            ,
            selectable: <?php echo $selectable; ?>,
            selectHelper: true,
            select: function(start, end, allDay) {
                
                $("#mensajeAlerta").addClass('hide');
                $("#mensajeAlertaSuccess").addClass('hide');
                var plantel_id = <?php echo $plantelPK->id; ?>;

                $.ajax({
                url: '/planificacion/planificacion/actualizarPlanificacion',
                data: 'plantel_id=' + plantel_id,
                type: "POST",
//                dataType: 'json',
                success: function(data) {
//                    alert(json.value);
                    $("#tipoMenu").html(data);
                    $("#menu").html('<option value="">- - -</option>');
                }
                });
                
                var title = 'Agregar Planificación';
                
                <?php
                    if($tipoMenu == null){
                ?>
                var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                    modal: true,
                    width: '600px',
                    dragable: false,
                    resizable: false,
                    //position: 'top',
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
                <?php
                }
                ?>
                <?php
                    if($tipoMenu != null){
                ?>
                var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                    modal: true,
                    width: '600px',
                    dragable: false,
                    resizable: false,
                    //position: 'top',
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                                $(this).dialog("close");
                            }

                        },
                        {
                            html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary btn-xs",
                            click: function() {
                                var menu_id = $("#menu").val();
                                registrar(start, end, allDay, menu_id, calendar);
                            }
                        }
                    ]
                });
                <?php
                }
                ?>

            }
            ,
            eventClick: function(calEvent, jsEvent, view) {
                

//        alert('Event: ' + calEvent.title);
//        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//        alert('View: ' + view.name);
                
                
//                console.log(calEvent);
                var openVista = '<div class="widget-box"><div class="widget-header"><h5>Planificaci&oacute;n</h5><div class="widget-toolbar"><a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a></div></div><div class="widget-body"><div class="widget-body-inner" style="display: block;"><div class="widget-main"><a href="#" class="search-button"></a><div style="display:block" class="search-form"><div class="widget-main form">';
                var closeVista = '</div><!-- search-form --></div><!-- search-form --></div></div></div></div>';
                var plantel_id = <?php echo $plantelPK->id; ?>;
                $.ajax({
                url: '/planificacion/planificacion/detallesPlanificacion',
                data: 'plantel_id=' + plantel_id + '&planificacion_id=' + calEvent.id,
                type: "POST",
                dataType: 'json',
                success: function(json) {
//                    alert(json.nombre);
//                    console.log(json);
                    $("#dialogPantalla").html(openVista + '<div class="row">\n\
                        <div class="col-sm-4">Menú Nutricional</div>\n\
                        <div class="col-sm-4">Tipo de Menú</div>\n\
                        <div class="col-sm-4">Calorías</div>\n\
                        <div class="col-sm-4"><input type="text" value="' + json.nombre + '" readOnly></div>\n\
                        <div class="col-sm-4"><input type="text" value="' + json.nombre + '" readOnly></div>\n\
                        <div class="col-sm-4"><input type="text" value="' + json.calorias + '" readOnly></div>\n\
                        <br><br><br>Preparación<br><div class="col-sm-12" style="border:1px solid #CCC;">' + json.preparacion + '</div></div>' + closeVista);
                        if(json.estatus == 'A'){
                            $("#eliminar").removeClass("hide");
                        }
                    }
                });
                
                var title = 'Consultar Planificación';
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '800px',
                    dragable: false,
                    resizable: false,
                    //position: 'top',
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                                $(this).dialog("close");
                            }

                        },
                        {
                            html: "<i class='icon-save info bigger-110'></i>&nbsp; Eliminar",
                            "class": "btn btn-primary btn-xs hide",
                            "id": "eliminar",
                            click: function() {
                                eliminar(calendar, calEvent);
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            }
        });
        calendar.fullCalendar('next');
        var date = calendar.fullCalendar('getDate');
        var mes = date.getMonth();
        var anio = date.getDay();
//        alert(mes);
        $("#inputMes").html(mes);
        $("#inputAnio").html(date);
    });
    
    function registrar(start, end, allDay, menu_id, calendar) {
        
        var menu_nutricional_id = menu_id; //Menu nutricional id
        var plantel_id = <?php echo $plantelPK->id; ?>;
        var start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
        var end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");
        var espacio = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        if(menu_id != '') {
        $.ajax({
            url: '/planificacion/planificacion/agregarEvento',
            data: 'menu_nutricional_id=' + menu_nutricional_id + '&plantel_id=' + plantel_id + '&start=' + start + '&end=' + end + '&allDay=' + allDay,
            type: "POST",
            dataType: 'json',
            success: function(json) {
                if(json.respuesta == 'exito'){
                    calendar.fullCalendar('renderEvent',
                        {
                            id: json.id,
                            title: json.title,
                            start: start,
                            end: start,
                            allDay: allDay,
                            editable: false,
                            className: json.nombreLabel,
                        },
                        true // make the event "stick"
                        );
                    $("#mensajeAlertaSuccess").removeClass('hide');
                    $("#mensajeAlerta").addClass('hide');
                    $("#mensajeAlertaSuccess").html(espacio + "Agregado con Éxito.<br><br>");
                }
                else if(json.respuesta == 'plantel'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "Este plantel no posee ingestas.<br><br>");
                }
                else if(json.respuesta == 'repetido'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "Ya existe una ingesta de este tipo al día seleccionado.<br><br>");
                }
                else if(json.respuesta == 'tope'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "Este plantel ya ha alcanzado el limite de ingesta por este día.<br><br>");
                }
                else if(json.respuesta == 'noExiste'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "No puedes registrar esta ingesta, este plantel no posee ingestas registradas.<br><br>");
                }
                else if(json.respuesta == 'mesDiferenteMenor'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "No puedes registrar la ingesta a este día, es un mes menor al mes activo.<br><br>");
                }
//                else if(json.respuesta == 'mesDiferenteMayor'){
//                    $("#mensajeAlerta").removeClass('hide');
//                    $("#mensajeAlertaSuccess").addClass('hide');
//                    $("#mensajeAlerta").html(espacio + "No puedes registrar esta ingesta a este día, es un mes superior al actual.<br><br>");
//                }
                else if(json.respuesta == 'cerrado'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "Lo sentimos, este mes ya fue cerrado.<br><br>");
                }
                else if(json.respuesta == 'diaFeriado'){
                    $("#mensajeAlerta").removeClass('hide');
                    $("#mensajeAlertaSuccess").addClass('hide');
                    $("#mensajeAlerta").html(espacio + "Lo sentimos, el día seleccionado es un día feriado.<br> " + espacio + json.descripcion  + "<br>");
                }
                calendar.fullCalendar('unselect');
                
                
                var plantel_id = <?php echo $plantelPK->id; ?>;

                $.ajax({
                url: '/planificacion/planificacion/actualizarPlanificacion',
                data: 'plantel_id=' + plantel_id,
                type: "POST",
//                dataType: 'json',
                success: function(data) {
//                    alert(json.value);
                    $("#tipoMenu").html(data);
                    $("#menu").html('<option value="">- - -</option>');
                }
                });
                
            }
        });
        }
        else {
            $("#mensajeAlerta").removeClass('hide');
            $("#mensajeAlertaSuccess").addClass('hide');
            $("#mensajeAlerta").html(espacio + "Hay campos vacios.<br><br>");
        }
    }

    function eliminar(calendar, calEvent) {
        $.ajax({
            url: '/planificacion/planificacion/eliminarPlanificacion',
            data: 'id=' + calEvent.id,
            type: "POST",
            success: function(json) {
//                alert('Eliminado con éxito.');
            }
        });
        calendar.fullCalendar('removeEvents', function(ev) {
            return (ev._id == calEvent._id);
        })
    }
    
</script>
<!--
<div id="inputMes"></div>
<div id="inputAnio"></div>
-->
<div id="dialogPantallaConsultar" class="hide">
    <div id="mensajeAlerta" class="errorDialogBox hide"></div>
    <div id="mensajeAlertaSuccess" class="successDialogBox hide"></div>
    <div id="plantel_id" class="hide"><?php echo $plantelPK->id; ?></div>
                <div class="row" id="formEventos">
            <?php
                if($tipoMenu == null){
                    ?>
                    <div class="errorDialogBox" id="mensajeAlerta">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        No puedes registrar una planificación, el plantel no posee ingestas registradas.
                        <br><br>
                    </div>
                    <?php
                }
                else if($tipoMenu != null){
            ?>
                    Tipo de Menú<br>
                    <select id="tipoMenu" class="col-sm-12">
                        <option value="">- - -</option>
                        <?php
                                foreach ($tipoMenu AS $t) {
                                    echo "<option value='" . $t['id'] . "'>" . $t['nombre'] . "</option>";
                                }
                        ?>
                    </select>
                    <br><br>
                    Menú<br>
                    <select id="menu" class="col-sm-12">
                        <option value="">- - -</option>
                    </select>
        <?php
            }
        ?>
                </div>
</div>
<div id="dialogPantalla" class="hide"></div>

<script>
    $('#tipoMenu').bind('change', function() {
        $("#mensajeAlerta").addClass('hide');
        $("#mensajeAlertaSuccess").addClass('hide');
        var id = $("#tipoMenu").val();
        $.ajax({
            url: '/planificacion/planificacion/obtenerMenu',
            data: 'id=' + id,
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if(data != null) {
                    $("#menu").html('<option value="">- - -<\/option>' + data.value);
                    $("#nombreLabel").html(data.label);
                }
                else {
                    $("#menu").html('<option value="">- - -<\/option>');
                    $("#nombreLabel").html('');
                }
            }
        });
    });
    
    function guardarCambios(plantel_id){
        var title = 'Guardar Cambios';
        var dialog = $("#dialogPantallaCambios").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            dragable: false,
            resizable: false,
            //position: 'top',
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }

                },
                {
                    html: "<i class='icon-save info bigger-110'></i>&nbsp; Cerrar Planificación",
                    "class": "btn btn-primary btn-xs",
                    "id": "guardarCambios",
                    click: function() {
                        $.ajax({
                            url: '/planificacion/planificacion/guardarCambios',
                            data: 'id=' + plantel_id + '&anio=' + anio + '&mes=' + mes,
                            type: "POST",
                            success: function(data) {
                                $("#dialogSuccess").removeClass('hide');
                                $("#dialogAlert").addClass('hide');
                                $("#guardarCambios").attr('onclick', false);
                                $("#btnGuardar").addClass('hide');
                            }
                        });
                    }
                }
            ]
        });
    }
    
</script>
<hr>
<div id="dialogPantallaCambios" class="hide">
    <div id="dialogSuccess" class="hide">
        <div class="successDialogBox">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cerrado con éxito.<br><br>
        </div>
    </div>
    <div id="dialogAlert" class="alertDialogBox">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Esta realmente seguro que desea procesar estos cambios?<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;una vez confirmado, no podrá editar la planificación de este mes.
    </div>
</div>
<div class="col-xs-6">
        <a id="btnRegresar" href="/planteles/" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
</div>
<?php
if($estatus == 0){
?>
<!--
<div align="right" class="col-xs-6">
    <a id="btnGuardar" onclick="guardarCambios(<?php echo $plantel_id; ?>);" class="btn btn-primary pull-right">
        Cerrar Planificación
        <i class="fa fa-save"></i>
    </a>
</div>
-->
<?php
}
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/fullcalendar.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/bootbox.min.js', CClientScript::POS_END);
?>
