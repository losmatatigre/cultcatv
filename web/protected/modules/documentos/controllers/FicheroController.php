<?php

class FicheroController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','upload','create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','upload','create'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','upload','create'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionUpload() {
      $carpeta='/public/uploads/archivos/';   

//        $options = null, $initialize = true, $error_messages = null, $filename=null, $id_model=''
        $upload_handler = new UploadHandler(null, true, null, date('YmdHis') . 'AT', $carpeta);
    }
	public function actionCreate(){
        $id = $this->getQuery('id');
        $model = new Fichero;
        if (isset($_POST['Fichero'])) { 
                $model->attributes = $_POST['Fichero']; 
                $model->usuario_ini = Yii::app()->user->id;
               $model->fecha = date("Y-m-d H:i:s");
                $model->url = $_POST['nombreArchivo'];
                //$model->nombre = (strlen(trim($_POST['nombreBD']))) ? $_POST['nombreBD'] : '';
                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog('ESCRITURA', 'documentos.fichero.create', 'EXITOSO', 'Se ha creado un nuevo archivo');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Se ha creado el tipo de notificacion'));
                        $model = new Fichero;
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                }
                
                
        }
            $this->render('_form', array('model'=>$model));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fichero']))
		{
			$model->attributes=$_POST['Fichero'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex() {
       $groupId = Yii::app()->user->group;
        $model = new Fichero('search');
        if (isset($_GET['Fichero']))
            $model->attributes = $_GET['Fichero'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('Fichero');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Fichero('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fichero']))
			$model->attributes=$_GET['Fichero'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fichero the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fichero::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fichero $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fichero-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
