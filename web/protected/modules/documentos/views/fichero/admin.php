<div class="widget-box">

    <div class="widget-header">
        <h5>Documentos</h5>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div id="div-usuario">

                    <div id="div-result-message" style="min-height: 60px;">

                    <?php
                    if (Yii::app()->user->pbac('userGroups.usuario.admin')):
?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo Yii::app()->createUrl('/documentos/fichero/create'); ?>" id="link-new" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i> &nbsp;
                                <span class="space-2">Registrar un Nuevo Video</span>
                            </a>
                        </div>
                        <br>
                    <?php
                    endif;
                    ?>

                    <div class="col-lg-12"><div class="space-6"></div></div>

                    <?php
                    /* echo CHtml::ajaxLink(Yii::t('userGroupsModule.admin', 'add user'),
                      Yii::app()->createUrl('/userGroups/admin/accessList', array('what'=>UserGroupsAccess::GROUP, 'id'=>'new')),
                      array('success'=>'js: function(data){ $("#user-detail").slideUp("slow", function(){ $("#user-detail").html(data).slideDown();}); }'),
                      array('id'=>'new-user-'.time())); */
                    ?>

                    <div class="clearfix margin-5">
                        <?php

                            $this->widget('zii.widgets.grid.CGridView', array(
                                'dataProvider' => $dataProvider,
                                'id' => 'user-grid',
                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                'enableSorting' => true,
                                'summaryText' => false,
                                'afterAjaxUpdate' => "

                                    function(){

                                        $('#UserGroupsUser_last_login').datepicker();
                                        $.datepicker.setDefaults($.datepicker.regional = {
                                            dateFormat: 'dd-mm-yy',
                                            showOn:'focus',
                                            showOtherMonths: false,
                                            selectOtherMonths: true,
                                            changeMonth: true,
                                            changeYear: true,
                                            minDate: new Date(1800, 1, 1),
                                            maxDate: 'today'
                                        });

                                        $('.look-data').unbind('click');
                                        $('.look-data').on('click',
                                            function(e) {
                                                e.preventDefault();
                                                var id = $(this).attr('data-id');
                                                ver(id);
                                            }
                                        );

                                        $('.change-status').unbind('click');
                                        $('.change-status').on('click',
                                            function(e) {
                                                e.preventDefault();
                                                var id = $(this).attr('data-id');
                                                var username = $(this).attr('data-username');
                                                var accion = $(this).attr('data-action');
                                                cambiarEstatus(id, username, accion);
                                            }
                                        );

                                        $('#UserGroupsUser_cedula').bind('keyup blur', function () {
                                            keyNum(this, false);
                                        });

                                        $('#UserGroupsUser_cedula').bind('blur', function () {
                                            clearField(this);
                                        });

                                        $('#UserGroupsUser_nombre').bind('keyup blur', function () {
                                            keyAlphaNum(this, true, true);
                                        });

                                        $('#UserGroupsUser_nombre').bind('blur', function () {
                                            clearField(this);
                                        });

                                        $('#UserGroupsUser_apellido').bind('keyup blur', function () {
                                            keyAlphaNum(this, true, true);
                                        });

                                        $('#UserGroupsUser_apellido').bind('blur', function () {
                                            clearField(this);
                                        });

                                        $('#UserGroupsUser_username').bind('keyup blur', function () {
                                            keyAlphaNum(this, true, true);
                                        });

                                        $('#UserGroupsUser_username').bind('blur', function () {
                                            clearField(this);
                                        });
                                    }
                                ",
                                //'selectionChanged' => 'function(id) { getPermission("' . Yii::app()->baseUrl . '", "' . UserGroupsAccess::GROUP . '", $.fn.yiiGridView.getSelection(id))}',
                                'columns' => array(
                                    array(
                                        'name'=>'nombre',
                                        'header'=>'nombre',
                                    ),
                                    array(
                                        'name'=>'fecha',
                                        'header'=>'fecha',
                                    ),
                                    array(
                                        'name'=>'url',
                                        'header'=>'url',
                                    ),
                                    array(
                                        'name'=>'extencion',
                                        'header'=>'extencion',
                                    ),
                                ),
                                'pager' => array(
                                    'header' => '',
                                    'htmlOptions'=>array('class'=>'pagination'),
                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                    'prevPageLabel'  => '<span title="Página Anterior">&#9668;</span>',
                                    'nextPageLabel'  => '<span title="Página Siguiente">&#9658;</span>',
                                    'lastPageLabel'  => '<span title="Última página">&#9658;&#9658;</span>',
                                ),
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>