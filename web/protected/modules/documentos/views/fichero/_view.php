<?php
/* @var $this FicheroController */
/* @var $data Fichero */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extencion')); ?>:</b>
	<?php echo CHtml::encode($data->extencion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act); ?>
	<br />


</div>