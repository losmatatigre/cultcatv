<?php

class OrdenCompraController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Permite consultar las Ordenes de Compra realizadas a este plantel',
        'write' => 'Permite Elaborar las Ordenes de Compra',
        'admin' => 'Permite Desactivar, Eliminar las Ordenes de Compra',
        'label' => 'Orden de Compra'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        //en esta seccion colocar los action de solo lectura o consulta
        return array(
            array('allow',
                'actions' => array(
                    'index', 'view'
                ),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'create',
                    'update'
                ),
                'pbac' => array('write', 'admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id) {
        $model = new OrdenCompra;
        $estatus = "";
        $idDecoded = base64_decode($id);
        $planificacionActiva = Planificacion::model()->planificacionActiva($idDecoded);
        $datos_plantel = PlantelPae::model()->datosPlantelOrdenCompra($idDecoded);
        if ($planificacionActiva) {
            if ($datos_plantel[0]["proveedor_nombre"] != null) {
                if ($datos_plantel[0]["pae_activo"] != null && $datos_plantel[0]["pae_activo"] == "SI") {
                    if ($datos_plantel[0]["orden_compra"] == null) {
                        if (isset($_POST['OrdenCompra'])) {

                            if (is_array($this->getPost('alimento')) and
                                    is_array($this->getPost('cantidad')) and
                                    is_array($this->getPost('precio'))
                            ) {
                                $arregloAlimento = $this->getPost('alimento');
                                $arregloCantidad = $this->getPost('cantidad');
                                $arregloPrecio = $this->getPost('precio');


                                if
                                (count($arregloAlimento) == count($arregloCantidad) and
                                        count($arregloCantidad) == count($arregloPrecio)
                                ) {


                                    $model->attributes = $_POST['OrdenCompra'];
                                    $model->usuario_ini_id = Yii::app()->user->id;
                                    $model->fecha_ini = date("Y-m-d H:i:s");
                                    $model->estatus = "A";
                                    $model->registrarOrden($model->attributes, $arregloAlimento, $arregloCantidad, $arregloPrecio);
                                    
                                 
                                } else {
                                    throw new CHttpException(404, 'Estimado Usuario, no se encontraron recursos solicitados.');
                                }
                            }else{
                                
                               throw new CHttpException(404, 'Estimado Usuario, no se encontro el recurso solicitado.');
                            }
                            
                        }
                        
                    } else {
                        $estatus = "con-orden-actual";
                    }
                } else {
                    $estatus = "sin-pae-act";
                }
            } else {
                $estatus = "sin-prov";
            }
        } else {
            $estatus = "sin-plan";
        }



        $this->render('create', array(
            'estatus' => $estatus,
            'model' => $model,
            'plantel_id' => $id,
            'datos_plantel' => $datos_plantel,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['OrdenCompra'])) {
            $model->attributes = $_POST['OrdenCompra'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new OrdenCompra('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OrdenCompra']))
            $model->attributes = $_GET['OrdenCompra'];


        $dataProvider = new CActiveDataProvider('OrdenCompra');
        $this->render('admin', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new OrdenCompra('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OrdenCompra']))
            $model->attributes = $_GET['OrdenCompra'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OrdenCompra the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = OrdenCompra::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function columnaSustitutos($data) {
        $id = $data["id"];
        $idEncode = $id;
        $id = base64_encode($id);


        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';


        if ($data["cantidadsustitutos"] > 0) {
            $sustituto = explode(',', $data["sustituto"]);
            $sustitutoCantidad = explode(',', $data["sustitutocantidad"]);
            
            $sustitutoPrecio = explode(',', $data["sustitutoprecio"]);
            $sustitutoTotal = explode(',', $data["sustitutototal"]);
            $sustitutoId = explode(',', $data["sustitutoid"]);
            for ($x = 0; $x < $data["cantidadsustitutos"]; $x++) {
                $sustitutoUm = explode('*', $sustituto[$x]);
                $columna .= "<li><a class = 'osito fa fa-plus blue', title = 'Visualizar este Proveedor', data-cantidad = '$sustitutoCantidad[$x]', data-um = '$sustitutoUm[1]', data-precio = '$sustitutoPrecio[$x]', data-id = '$id', data-alim='$sustitutoId[$x]' , data-nombre = '$sustitutoUm[0]', data-total = '$sustitutoTotal[$x]' ><span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;" . $sustitutoUm[0] . "</span></a></li>";
            }
        } else {
            $columna .= '<li>' . "<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp; No tiene sustitutos </span></li>";
        }


        $columna .= '</ul></div>';
        return $columna;
    }

    /**
     * Performs the AJAX validation.
     * @param OrdenCompra $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'orden-compra-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

