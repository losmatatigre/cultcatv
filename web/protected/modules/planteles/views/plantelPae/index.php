<?php
/* @var $this PlantelPaeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plantel Paes',
);

$this->menu=array(
	array('label'=>'Create PlantelPae', 'url'=>array('create')),
	array('label'=>'Manage PlantelPae', 'url'=>array('admin')),
);
?>

<h1>Plantel Paes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
