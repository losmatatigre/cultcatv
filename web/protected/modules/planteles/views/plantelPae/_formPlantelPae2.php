<?php
/* @var $this PlantelPaeController */
/* @var $data PlantelPae */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel_id')); ?>:</b>
	<?php echo CHtml::encode($data->plantel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus_plantel_pae')); ?>:</b>
	<?php echo CHtml::encode($data->estatus_plantel_pae); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_desde')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_desde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ultima')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ultima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matricula_general')); ?>:</b>
	<?php echo CHtml::encode($data->matricula_general); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('simoncito')); ?>:</b>
	<?php echo CHtml::encode($data->simoncito); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_elim')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_elim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?>:</b>
	<?php echo CHtml::encode($data->estatus); ?>
	<br />

	*/ ?>

</div>