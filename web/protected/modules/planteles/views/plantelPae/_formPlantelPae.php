<?php
/* @var $this PlantelPaeController */
/* @var $model PlantelPae */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'plantel-pae-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'plantel_id'); ?>
		<?php echo $form->textField($model,'plantel_id'); ?>
		<?php echo $form->error($model,'plantel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pae_activo'); ?>
		<?php echo $form->textField($model,'pae_activo',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'pae_activo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_inicio'); ?>
		<?php echo $form->textField($model,'fecha_inicio'); ?>
		<?php echo $form->error($model,'fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ultima_actualizacion'); ?>
		<?php echo $form->textField($model,'fecha_ultima_actualizacion'); ?>
		<?php echo $form->error($model,'fecha_ultima_actualizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'matricula_general'); ?>
		<?php echo $form->textField($model,'matricula_general'); ?>
		<?php echo $form->error($model,'matricula_general'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'posee_simoncito'); ?>
		<?php echo $form->textField($model,'posee_simoncito',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'posee_simoncito'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id'); ?>
		<?php echo $form->error($model,'usuario_ini_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini'); ?>
		<?php echo $form->error($model,'fecha_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id'); ?>
		<?php echo $form->error($model,'usuario_act_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act'); ?>
		<?php echo $form->error($model,'fecha_act'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim'); ?>
		<?php echo $form->error($model,'fecha_elim'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'estatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_servicio_pae_id'); ?>
		<?php echo $form->textField($model,'tipo_servicio_pae_id'); ?>
		<?php echo $form->error($model,'tipo_servicio_pae_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'matricula_pequena'); ?>
		<?php echo $form->textField($model,'matricula_pequena'); ?>
		<?php echo $form->error($model,'matricula_pequena'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'matricula_mediana'); ?>
		<?php echo $form->textField($model,'matricula_mediana'); ?>
		<?php echo $form->error($model,'matricula_mediana'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'matricula_grande'); ?>
		<?php echo $form->textField($model,'matricula_grande'); ?>
		<?php echo $form->error($model,'matricula_grande'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'posee_area_cocina'); ?>
		<?php echo $form->textField($model,'posee_area_cocina',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'posee_area_cocina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_servicio_id'); ?>
		<?php echo $form->textField($model,'condicion_servicio_id'); ?>
		<?php echo $form->error($model,'condicion_servicio_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->