<?php
/* @var $this PlantelPaeController */
/* @var $model PlantelPae */

$this->breadcrumbs=array(
	'Plantel Paes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PlantelPae', 'url'=>array('index')),
	array('label'=>'Create PlantelPae', 'url'=>array('create')),
	array('label'=>'View PlantelPae', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PlantelPae', 'url'=>array('admin')),
);
?>

<h1>Update PlantelPae <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>