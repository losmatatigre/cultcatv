<?php
/* @var $this PlantelPaeController */
/* @var $model PlantelPae */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'plantel_id'); ?>
		<?php echo $form->textField($model,'plantel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pae_activo'); ?>
		<?php echo $form->textField($model,'pae_activo',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_inicio'); ?>
		<?php echo $form->textField($model,'fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_ultima_actualizacion'); ?>
		<?php echo $form->textField($model,'fecha_ultima_actualizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_general'); ?>
		<?php echo $form->textField($model,'matricula_general'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'posee_simoncito'); ?>
		<?php echo $form->textField($model,'posee_simoncito',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_servicio_pae_id'); ?>
		<?php echo $form->textField($model,'tipo_servicio_pae_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_pequena'); ?>
		<?php echo $form->textField($model,'matricula_pequena'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_mediana'); ?>
		<?php echo $form->textField($model,'matricula_mediana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matricula_grande'); ?>
		<?php echo $form->textField($model,'matricula_grande'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'posee_area_cocina'); ?>
		<?php echo $form->textField($model,'posee_area_cocina',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condicion_servicio_id'); ?>
		<?php echo $form->textField($model,'condicion_servicio_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->