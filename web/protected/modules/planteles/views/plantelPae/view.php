<?php
/* @var $this PlantelPaeController */
/* @var $model PlantelPae */

$this->breadcrumbs=array(
	'Plantel Paes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PlantelPae', 'url'=>array('index')),
	array('label'=>'Create PlantelPae', 'url'=>array('create')),
	array('label'=>'Update PlantelPae', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PlantelPae', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PlantelPae', 'url'=>array('admin')),
);
?>

<h1>View PlantelPae #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'plantel_id',
		'pae_activo',
		'fecha_inicio',
		'fecha_ultima_actualizacion',
		'matricula_general',
		'posee_simoncito',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'tipo_servicio_pae_id',
		'matricula_pequena',
		'matricula_mediana',
		'matricula_grande',
		'posee_area_cocina',
		'condicion_servicio_id',
	),
)); ?>
