<?php
/* @var $this PlantelPaeController */
/* @var $model PlantelPae */

$this->breadcrumbs=array(
	'Plantel Paes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PlantelPae', 'url'=>array('index')),
	array('label'=>'Manage PlantelPae', 'url'=>array('admin')),
);
?>

<h1>Create PlantelPae</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>