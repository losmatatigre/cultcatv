<div class="widget-box">

    <div class="widget-header">
        <h5>
            <i class="fa fa-sitemap"></i>
            Proyectos endogenos "<?php echo $model->nombre;?>"
        </h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display: block;" class="widget-body-inner">
            <div class="widget-main">

                <?php echo CHtml::link('', '#', array('class' => 'search-button')); ?>
                <div class="search-form" style="display:block">

                <?php
                    $resultado = ProyectosEndogenos::model()->obtenerDesarrolloEndogenosPlantel($plantel_id);
                ?>
                
                <table class="table table-striped table-bordered table-hover" style="width:300px;">
                    <thead>
                    
                    <tr>
                        <th id="proyectos_endogenos_usados_c0">
                            <center>
                                <b>Proyectos end&oacute;genos</b>
                            </center>
                        </th>
                    </tr>

                    </thead>

                    <tbody>
                        <?php
                        foreach ($resultado as $result)
                        {
                        echo '
                            <tr class="odd">
                                <td>
                                    <div>
                                        '.$result['nombre'].'
                                    </div>
                                </td>
                            </tr>
                            ';

                        }
                        if($resultado == NULL)
                        {
                            echo
                            '
                                <tr>
                                    <td>
                                        Este plantel no posee Proyecto Endogeno.
                                    </td>
                                </tr>
                            ';
                        }
                    ?>
                    
                    
                    </tbody>
                </table>



                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>