<div class="widget-box">

    <div class="widget-header">
        <h5>
            <i class="fa fa-thumb-tack"></i>
            Servicios "<?php echo $model->nombre;?>"
        </h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display: block;" class="widget-body-inner">
            <div class="widget-main">

                <?php echo CHtml::link('', '#', array('class' => 'search-button')); ?>
                <div class="search-form" style="display:block">
                
                <?php
                    $resultado = ServicioPlantel::model()->obtenerServiciosPlantel($plantel_id);
                ?>

                <table class="table table-striped table-bordered table-hover" style="width:600px;">
                    <thead>
                    
                    <tr>
                        <th>
                            <center>
                                <b>Servicio</b>
                            </center>
                        </th>
                        <th>
                            <center>
                                <b>Desde</b>
                            </center>
                        </th>
                        <th>
                            <center>
                                <b>Condici&oacute;n</b>
                            </center>
                        </th>
                    </tr>

                    </thead>

                    <tbody>
                        <?php
                        foreach ($resultado as $result)
                        {
                        echo '
                            <tr class="odd">
                                <td>
                                    <div>
                                        '.$result['servicio'].'
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        '.$result['fecha_desde'].'
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        '.$result['nombre'].'
                                    </div>
                                </td>
                            </tr>
                            ';

                        }
                        if($resultado == NULL)
                        {
                            echo
                            '
                                <tr>
                                    <td colspan="3">
                                        No se encuentran servicios registrados en este plantel.
                                    </td>
                                </tr>
                            ';
                        }
                    ?>
                    
                    
                    </tbody>
                </table>

                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>