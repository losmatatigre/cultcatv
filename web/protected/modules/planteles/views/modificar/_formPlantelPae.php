<div class="widget-box">
    <div class="widget-header">
        <h5>Datos del PAE</h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="">
            <div class="widget-main">

                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div class="widget-main form">
                        
                        

<div class="form">
    
    
<div class="successDialogBox hide" id="mensajeAlertaExito">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estatus cambiado con éxito.<br><br></div>
<div class="errorDialogBox hide" id="mensajeAlertaError"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'plantel-pae-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

<?php
    if($model->pae_activo != null){
        $requerido = '';
    }
    else if($model->pae_activo == null){
        $requerido = '<span class="required">*</span>';
    }
?>

    <div class="row">

	<div class="col-sm-4">
            <?php echo $form->hiddenField($model,'id'); ?>
            <?php echo $form->hiddenField($model,'plantel_id'); ?>
            <label>¿El PAE se encuentra activo?</label>
            <?php echo $form->textField($model,'pae_activo',array('size'=>2,'maxlength'=>2, 'class' => 'span-12', 'disabled' => 'disabled')); ?>
	</div>

	<div class="col-sm-4">
            <label>Tipo de Servicio <?php echo $requerido; ?></label>
            <?php // echo $model->tipoServicioPae->nombre; ?>
            <?php // echo $form->textField($model,'tipo_servicio_pae_id'); ?>
            <?php
            $disabled = '';
            if($model->tipo_servicio_pae_id != ''){
                $disabled = 'disabled';
            }
            echo $form->dropDownList(
                    $model, 'tipo_servicio_pae_id', CHtml::listData(TipoServicioPae::model()->findAll(), 'id', 'nombre'), array('empty' => array('' => '- - -'), 'class' => 'span-12', 'disabled' => $disabled)
            );
            ?>
	</div>

	<div class="col-sm-4">
            <label>¿Posee Simoncito? <?php echo $requerido; ?></label>
            <?php
            echo $form->dropDownList(
                    $model, 'posee_simoncito', CHtml::listData(array(array('id' => 'SI', 'nombre' => 'SI'), array('id' => 'NO', 'nombre' => 'NO')), 'id', 'nombre'  ), array('empty' => array('' => '- - -'), 'class' => 'span-12')
            );
            ?>
	</div>
        
    </div>
    <br>
    <div class="row">
        
	<div class="col-sm-4">
            <label>Fecha de Inicio</label>
            <?php
//            var_dump($model->id);
            $fecha_inicio = '';
            $fecha_ultima_actualizacion = '';
            if($model->id != null){
                $fecha_inicio = date_create($model->fecha_inicio);
                $fecha_inicio = date_format($fecha_inicio,"d-m-Y H:i:s");
            }
            ?>
            <input type="text" name="fecha_inicio" value="<?php echo $fecha_inicio; ?>" id="fecha_inicio" class="span-12" disabled="disabled">
	</div>

	<div class="col-sm-4">
            <label>Ultima fecha de Actualización</label>
            <?php
            if($model->id != null){
                $fecha_ultima_actualizacion = date_create($model->fecha_ultima_actualizacion);
                $fecha_ultima_actualizacion = date_format($fecha_ultima_actualizacion,"d-m-Y H:i:s");
            }
            ?>
            <input type="text" name="[fecha_ultima_actualizacion" value="<?php echo $fecha_ultima_actualizacion; ?>" id="PlantelPae_fecha_ultima_actualizacion" class="span-12" disabled="disabled">
	</div>

	<div class="col-sm-4">
            <label>Matricula General</label>
            <?php echo $form->textField($model,'matricula_general', array('class' => 'span-12', 'disabled' => 'disabled')); ?>
	</div>
        
    </div>
    <br>
    
    <div class="row">

	<div class="col-sm-4">
            <label>¿Posee area de cocina? <?php echo $requerido; ?></label>
            <?php
            echo $form->dropDownList(
                    $model, 'posee_area_cocina', CHtml::listData(array(array('id' => 'SI', 'nombre' => 'SI'), array('id' => 'NO', 'nombre' => 'NO')), 'id', 'nombre'  ), array('empty' => array('' => '- - -'), 'class' => 'span-12')
            );
            ?>
	</div>

	<div class="col-sm-4">
            <label>Condición de Servicio <?php echo $requerido; ?></label>
            <?php
            echo $form->dropDownList(
                    $model, 'condicion_servicio_id', CHtml::listData(CondicionServicio::model()->findAll(), 'id', 'nombre'  ), array('empty' => array('' => '- SELECCIONE -'), 'class' => 'span-12')
            );
            ?>
	</div>
        
    </div>

<?php $this->endWidget(); ?>

</div><!-- form --> 
                        
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>

<hr>
<div class="col-sm-12">
    <?php
    if($model->pae_activo == 'SI'){
        $class = 'danger';
        $value = 'Inactivar';
        $icon = 'down';
    }
    elseif($model->pae_activo == 'NO'){
        $class = 'success';
        $value = 'Activar';
        $icon = 'up';
    }
    else{
        $class = '... hide';
        $value = '...';
        $icon = '...';
    }
    ?>
    <div class="col-sm-6"></div>
    <div class="col-sm-6" align="right">
        <?php
        if($model->pae_activo != null){
        ?>
            <a class="btn btn-<?php echo $class; ?>" onclick="accion(<?php echo $model->plantel_id; ?>, '<?php echo $value; ?>', '<?php echo $class; ?>', '<?php echo $icon; ?>', '<?php echo $model->pae_activo; ?>');" id="btnEstatusAccion">
                <?php echo $value; ?>
                <i class="fa fa-arrow-<?php echo $icon; ?>"></i>
            </a>
            &nbsp;&nbsp;
            <a class="btn btn-info" onclick="accion(<?php echo $model->plantel_id; ?>, '<?php echo $value; ?>', '<?php echo $class; ?>', '<?php echo $icon; ?>', '<?php echo $model->pae_activo; ?>');" id="btnEstatusAccion">
                Guardar
                <i class="fa fa-save"></i>
            </a>
        <?php
        }
        else if($model->pae_activo == null){
        ?>
            <a class="btn btn-success" onclick="accionActivar(<?php echo base64_decode($_REQUEST['id']); ?>);" id="btnEstatusAccion">
                Activar
                <i class="fa fa-arrow-up"></i>
            </a>
        <?php
        }
        ?>
    </div>
</div>
<br><br><br>

<div id="dialogPantallaObservacion" class="hide">
    <div class="alertDialogBox" id="mensajeAlertaInfo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Indique el motivo detalladamente por el cual quiere realizar esta acción.<br><br></div>
    <div class="errorDialogBox hide" id="mensajeAlerta">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hay campos vacios que son requeridos.<br><br></div>
    <div class="row">
        <div id="motivoEstatus" class="hide">
            <b>Motivo</b> <span class="required">*</span><br>
            <select class="col-sm-12" id="motivo_inactividad">
                <option value="">- - -</option>
                <?php
                $motivos = MotivoInactividadPae::model()->findAll();
                foreach($motivos AS $motivo){
                    echo '<option value="' . $motivo['id'] . '"> ' . $motivo["nombre"] . '</option>';

                }
                ?>
            </select><br><br>
        </div>
        <b>Observación</b> <span class="required">*</span><br>
        <textarea id="razonAccion" class="col-sm-12"></textarea>
    </div>
</div>


<div id="confirmacion" class="hide">
    <div class="alertDialogBox" id="mensajeAlertaInfo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>¿Estas seguro que deseas activar servicios del PAE?.</b><br><br></div>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/plantelPae.js', CClientScript::POS_END);
?>

<script>
    function accionActivar(plantel_id){
        
        $("#mensajeAlertaError").addClass('hide');
        
        var tipo_servicio_pae_id = $("#PlantelPae_tipo_servicio_pae_id").val();
        var posee_simoncito = $("#PlantelPae_posee_simoncito").val();
        var posee_area_cocina = $("#PlantelPae_posee_area_cocina").val();
        var condicion_servicio_id = $("#PlantelPae_condicion_servicio_id").val();
        var espacio = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//        var espacio = '';
        var mensaje = espacio + '';
        mensaje = mensaje + '<ul><b>Corrige los siguientes errores:</b><br><br>';
        if(tipo_servicio_pae_id == ''){
            mensaje = mensaje + '<li>El Tipo de Servicio no puede estar vacio.</li>';
        }
        if(posee_simoncito == ''){
            mensaje = mensaje + '<li>El Posee Simoncito no puede estar vacio.</li>';
        }
        if(posee_area_cocina == ''){
            mensaje = mensaje + '<li>El Posee Area de Cocina no puede estar vacio.</li>';
        }
        if(condicion_servicio_id == ''){
            mensaje = mensaje + '<li>El Condicion de Servicio no puede estar vacio.</li>';
        }
        mensaje = mensaje + '</ul>';
        
        //alert(mensaje);
        if(mensaje != espacio + '<ul><b>Corrige los siguientes errores:</b><br><br></ul>'){
            $("html, body").animate({scrollTop: 0}, "fast");
            $("#mensajeAlertaError").removeClass('hide');
            $("#mensajeAlertaError").html(mensaje);
        }
        else{
        
            var dialog = $("#confirmacion").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggale: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar servicios del PAE </h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Activar <i class='fa fa-arrow-up bigger-110'></i>",
                        "class": "btn btn-success btn-xs",
                        "id": "botonAccionActivar",
                        click: function() {
                            
                            var servicio_pae = $("#PlantelPae_tipo_servicio_pae_id").val();
                            var posee_simoncito = $("#PlantelPae_posee_simoncito").val();
                            var posee_area_cocina = $("#PlantelPae_posee_area_cocina").val();
                            var condicion_servicio = $("#PlantelPae_condicion_servicio_id").val();
                            
                            $.ajax({
                                url: "/planteles/modificar/activarPae",
                                data: 'plantel_id=' + plantel_id +
                                        '&servicio_pae=' + servicio_pae +
                                        '&posee_simoncito=' + posee_simoncito +
                                        '&posee_area_cocina=' + posee_area_cocina +
                                        '&condicion_servicio=' + condicion_servicio,
                                type: 'post',
                                dataType: 'json',
                                success: function(json) {
                                    $("#PlantelPae_pae_activo").val(json.pae_activo);
                                    $("#PlantelPae_tipo_servicio_pae_id").val(json.tipo_servicio);
                                    $("#PlantelPae_posee_simoncito").val(json.posee_simonsito);
                                    $("#fecha_inicio").val(json.fecha_inicio);
                                    $("#PlantelPae_fecha_ultima_actualizacion").val(json.fecha_ultima);
                                    $("#PlantelPae_matricula_general").val(json.matricula_general);
                                    $("#PlantelPae_posee_area_cocina").val(json.posee_area_cocina);
                                    $("#PlantelPae_condicion_servicio_id").val(json.condicion_servicio);

                                    $("html, body").animate({scrollTop: 0}, "fast");
                                    $("#dialogPantallaObservacion").dialog("close");
                                    $("#mensajeAlertaExito").removeClass('hide');
                                    $("#confirmacion").dialog("close");
                                }
                                });
                        }
                    }
                ]
            });
        }
    }
</script>