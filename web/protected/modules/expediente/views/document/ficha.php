<?php $this->renderPartial('_form');?>
<br>
<br>
<div class='col-md-6'>
    <table class='table-hover'>
        <caption style="font-size: 15px; margin-left: 5px;"><strong> Datos Personales </strong></caption>
        <tr>
            <td>
                <?php echo $resultadoFoto['url'];
                
                ?>
                
                </td>
        </tr>
        <?php
        foreach ($resultado as $data) {
            ?>
            <tr>
                <td>
                    <div class="profile-info-row">
                        <div class="profile-user-info profile-user-info-striped">
                            <div class="profile-info-name"><b><i> <?php echo $data['etiqueta']; ?> </i></b></div>
                            <div class="profile-info-value">
                                <span id="nombre_zona"> <?php
        if ($data['etiqueta'] == 'Fecha Nacimiento' or $data['etiqueta'] == 'Fecha de Ingreso') {
            echo date("d/m/Y", strtotime($data['valor']));
        } else {
            echo $data['valor'];
        }
            ?> </span>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

<div class='col-md-6'>
    <table class='table-hover'>
        <caption style="font-size: 15px; margin-left: 5px;"><strong> Archivos </strong></caption>
        <?php
        
        foreach ($resultadoTexto AS $data) {
            $tipo_documento = $data['ext_acceso'];
            if($tipo_documento==1)
                $tipo_documento='Expedientes Administrativos';
            else 
                 $tipo_documento='Expedientes Laboral';
            ?>

            <tr>
                <td>
                    <div class="profile-info-row">
                        <div class="profile-user-info profile-user-info-striped">
                            <div class="profile-info-name"<b><i> <?php echo $tipo_documento;?> </i></b></div>
                            <div class="profile-info-value">
                                <span id="nombre_zona"> <?php echo $data['url']; ?> </span>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

<?php } ?>

</table>

