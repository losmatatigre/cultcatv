<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->pageTitle = 'Registro De Documentos';

      $this->breadcrumbs=array(
	'Documents'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>