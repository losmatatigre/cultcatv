<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->breadcrumbs = array(
    'Documentos',
);
$this->pageTitle = 'Administración de Documentos';
?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Expedientes</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En Este Módulo Podrá Realizar La Búsqueda Por Cédula, Nombre, Apellido Ó Número De Expediente.
                            </p>
                        </div>
                    </div>
                
                
                <div>        

<?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'document-form',
                                //'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

<?php echo '<input type="hidden" id="zona_id"  name="zona_id"/>'; ?>
                            <div class="col-md-12" id ="">
                                <div class="col-md-5">
                                    <div class="col-md-12"><label for="Plantel_cedula">Cédula<span class="required">*</span></label></div>
                                    <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="0123456789" title="Ej: 99999999"   style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="cedula"/>'; ?>
                                    <?php echo CHtml::button('Busqueda', array('class'=>'btn btn-info btn-xs','submit' => array('document/busqueda'))); ?>

                                </div>
                            </div>
                    
<?php echo '<input type="hidden" id="zona_id"  name="zona_id"/>'; ?>
                            <div class="col-md-12" id ="">
                                <div class="col-md-5">
                                    <div class="col-md-12"><label for="Plantel_cedula">Número De Expediente<span class="required">*</span></label></div>
                                    <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="0123456789" title="Ej: 99999999"   style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="cedula"/>'; ?>
                                    <?php echo CHtml::button('Busqueda', array('class'=>'btn btn-info btn-xs','submit' => array('document/busqueda'))); ?>

                                </div>
                            </div>
                    
<?php echo '<input type="hidden" id="zona_id"  name="zona_id"/>'; ?>
                            <div class="col-md-12" id ="">
                                <div class="col-md-5">
                                    <div class="col-md-12"><label for="Plantel_cedula">Nombres y Apellidos<span class="required">*</span></label></div>
                                    <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="ABCDEFHIJKL" title="Ej: ABCDEFHIJKL"   style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="cedula"/>'; ?>
                                    <?php echo CHtml::button('Busqueda', array('class'=>'btn btn-info btn-xs','submit' => array('document/busqueda'))); ?>

                                </div>
                            </div>
<?php
 $this->endWidget();?>

                                       <br>
                    <br>
                     <br>
                            </div>
                </div>
            </div>
        </div>