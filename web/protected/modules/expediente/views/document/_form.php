<div id="validaciones"></div>
<?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'document-form',
                                'enableAjaxValidation'=>false,
                        )); ?>
<?php echo '<input type="hidden" id="zona_id"  name="zona_id"/>'; ?>
                            <div class="col-md-12" id ="">
                                <div class="col-md-5">
                                    <div class="col-md-12"><label for="Plantel_cedula">Cédula<span class="required">*</span></label></div>
                                    <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="0123456789" title="Ej: 99999999" style="padding:3px 4px" maxlength="10" size="10" id="cedula" class="span-6" name="cedula"/>'; ?>
                                    <?php echo CHtml::button('Busqueda', array('class'=>'btn btn-info btn-xs','submit' => array('document/busqueda'))); ?>

                                </div>
                            </div>


                        <?php $this->endWidget();
?>
<script type="text/javascript">
       $('#cedula').bind('keyup blur', function() {
           keyNum(this, true);
       });
       $('#cedula').bind('blur', function() {
           clearField(this);
       });
     $("#yt0").click(function() {
       var cedula = $.trim($('#cedula').val());
       if (cedula == "") {
           displayDialogBox('validaciones', 'error', 'Datos Faltantes: El Campo Cédula No Puede Ser Vacio.');
           return false;
       }
   });
</script>
</script>               