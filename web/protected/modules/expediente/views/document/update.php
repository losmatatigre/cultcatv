<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->pageTitle = 'Actualización De Datos De Documentos';

      $this->breadcrumbs=array(
	'Documents'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>