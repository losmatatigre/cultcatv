<?php

/* @var $this DocumentController */
/* @var $model Document */

$this->breadcrumbs=array(
	'Documents'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración De Documentos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Lista De Documentos</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En Este Módulo Podrá Registrar Y/O/U Actualizar Los Datos De Los Documentos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/document/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Documentos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'document-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>acceso</center>',
            'name' => 'acceso',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[acceso]', $model->acceso, array('title' => '',)),
        ),
        array(
            'header' => '<center>ext_acceso</center>',
            'name' => 'ext_acceso',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[ext_acceso]', $model->ext_acceso, array('title' => '',)),
        ),
        array(
            'header' => '<center>jerarquia</center>',
            'name' => 'jerarquia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[jerarquia]', $model->jerarquia, array('title' => '',)),
        ),
        array(
            'header' => '<center>ubicacion</center>',
            'name' => 'ubicacion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[ubicacion]', $model->ubicacion, array('title' => '',)),
        ),
        array(
            'header' => '<center>tipo_liter</center>',
            'name' => 'tipo_liter',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[tipo_liter]', $model->tipo_liter, array('title' => '',)),
        ),
        array(
            'header' => '<center>nivel_bibl</center>',
            'name' => 'nivel_bibl',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[nivel_bibl]', $model->nivel_bibl, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>nivel_reg</center>',
            'name' => 'nivel_reg',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[nivel_reg]', $model->nivel_reg, array('title' => '',)),
        ),
        array(
            'header' => '<center>tipo_ref_analitica</center>',
            'name' => 'tipo_ref_analitica',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[tipo_ref_analitica]', $model->tipo_ref_analitica, array('title' => '',)),
        ),
        array(
            'header' => '<center>paginas</center>',
            'name' => 'paginas',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[paginas]', $model->paginas, array('title' => '',)),
        ),
        array(
            'header' => '<center>volumen</center>',
            'name' => 'volumen',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[volumen]', $model->volumen, array('title' => '',)),
        ),
        array(
            'header' => '<center>volumen_final</center>',
            'name' => 'volumen_final',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[volumen_final]', $model->volumen_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>numero</center>',
            'name' => 'numero',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[numero]', $model->numero, array('title' => '',)),
        ),
        array(
            'header' => '<center>numero_final</center>',
            'name' => 'numero_final',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[numero_final]', $model->numero_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>numeracion</center>',
            'name' => 'numeracion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[numeracion]', $model->numeracion, array('title' => '',)),
        ),
        array(
            'header' => '<center>enum_nivel_ini_3</center>',
            'name' => 'enum_nivel_ini_3',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[enum_nivel_ini_3]', $model->enum_nivel_ini_3, array('title' => '',)),
        ),
        array(
            'header' => '<center>enum_nivel_ini_4</center>',
            'name' => 'enum_nivel_ini_4',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[enum_nivel_ini_4]', $model->enum_nivel_ini_4, array('title' => '',)),
        ),
        array(
            'header' => '<center>enum_nivel_fin_3</center>',
            'name' => 'enum_nivel_fin_3',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[enum_nivel_fin_3]', $model->enum_nivel_fin_3, array('title' => '',)),
        ),
        array(
            'header' => '<center>enum_nivel_fin_4</center>',
            'name' => 'enum_nivel_fin_4',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[enum_nivel_fin_4]', $model->enum_nivel_fin_4, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_editor</center>',
            'name' => 'cod_editor',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[cod_editor]', $model->cod_editor, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_editor_inst</center>',
            'name' => 'cod_editor_inst',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[cod_editor_inst]', $model->cod_editor_inst, array('title' => '',)),
        ),
        array(
            'header' => '<center>edicion</center>',
            'name' => 'edicion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[edicion]', $model->edicion, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_iso</center>',
            'name' => 'fecha_iso',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[fecha_iso]', $model->fecha_iso, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_iso_final</center>',
            'name' => 'fecha_iso_final',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[fecha_iso_final]', $model->fecha_iso_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_pub</center>',
            'name' => 'fecha_pub',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[fecha_pub]', $model->fecha_pub, array('title' => '',)),
        ),
        array(
            'header' => '<center>isbn</center>',
            'name' => 'isbn',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[isbn]', $model->isbn, array('title' => '',)),
        ),
        array(
            'header' => '<center>indice_suplemento</center>',
            'name' => 'indice_suplemento',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[indice_suplemento]', $model->indice_suplemento, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_conf</center>',
            'name' => 'cod_conf',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[cod_conf]', $model->cod_conf, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_proy</center>',
            'name' => 'cod_proy',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[cod_proy]', $model->cod_proy, array('title' => '',)),
        ),
        array(
            'header' => '<center>disemina</center>',
            'name' => 'disemina',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[disemina]', $model->disemina, array('title' => '',)),
        ),
        array(
            'header' => '<center>url</center>',
            'name' => 'url',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[url]', $model->url, array('title' => '',)),
        ),
        array(
            'header' => '<center>n_disk_cin</center>',
            'name' => 'n_disk_cin',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[n_disk_cin]', $model->n_disk_cin, array('title' => '',)),
        ),
        array(
            'header' => '<center>sala</center>',
            'name' => 'sala',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[sala]', $model->sala, array('title' => '',)),
        ),
        array(
            'header' => '<center>impresion</center>',
            'name' => 'impresion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[impresion]', $model->impresion, array('title' => '',)),
        ),
        array(
            'header' => '<center>orden</center>',
            'name' => 'orden',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[orden]', $model->orden, array('title' => '',)),
        ),
        array(
            'header' => '<center>cerrado</center>',
            'name' => 'cerrado',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[cerrado]', $model->cerrado, array('title' => '',)),
        ),
        array(
            'header' => '<center>hora_iso_inicio</center>',
            'name' => 'hora_iso_inicio',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[hora_iso_inicio]', $model->hora_iso_inicio, array('title' => '',)),
        ),
        array(
            'header' => '<center>hora_iso_final</center>',
            'name' => 'hora_iso_final',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[hora_iso_final]', $model->hora_iso_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Document[id]', $model->id, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>