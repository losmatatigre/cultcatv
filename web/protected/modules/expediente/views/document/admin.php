<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->breadcrumbs = array(
    'Documentos',
);
$this->pageTitle = 'Administración de Documentos';
?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Expedientes</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En Este Módulo Podrá Realizar La Búsqueda.
                            </p>
                        </div>
                    </div>
                
                
                <div>                   
                           

 <?php $this->renderPartial('_form');
                            ?>
                    <br>
                    <br>
                     <br>
                            </div>
                </div>
            </div>
        </div>