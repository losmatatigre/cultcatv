<?php

class TituloController extends Controller {

    /**
     * @ Var string el diseño predeterminado de las opiniones. Por defecto es '/ / layouts/column2', significado
     * Usando diseño de dos columnas. Ver 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @ Return array filtros de acción
     */
    static $_permissionControl = array(
        'write' => 'Gestion Prueba de Titulos',
        'admin' => 'Admin Prueba de Titulos',
        'label' => 'Módulo de Pueba de Titulo'
    );

    /**
     * @Return array filtros de acción
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    /**
     * Especifica las reglas de control de acceso.
     * Este método es utilizado por el filtro 'AccessControl'.
     * Reglas de control de acceso a una matriz @ return
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
       //$this->renderPartial('vista_prueba');
        $query = "SELECT titulo.registro_serial(ARRAY['5134251522', '51215361273'], 'titulo.registro.serial', '20140529-REGTIT.ods', 1, '172.16.3.47') AS result";
        $consulta = Yii::app()->db->createCommand($query);
        $traer_datos = $consulta->queryRow();
        $datos_decoded = $this->pg_array_parse($traer_datos['result'], array());
        $this->render('index',array('datos_decoded'=>$datos_decoded));
    }
    function pg_array_parse($text, $output, $limit = false, $offset = 1){
        if (false === $limit){
            $limit = strlen($text) - 1;
            $output = array();
        }
        if ('{}' != $text)
            do {
                if ('{' != $text{$offset}) {
                    preg_match("/(\\{?\"([^\"\\\\]|\\\\.)*\"|[^,{}]+)+([,}]+)/", $text, $match, 0, $offset);

                    $offset += strlen($match[0]);
                    $output[] = ( '"' != $match[1]{0} ? $match[1] : stripcslashes(substr($match[1], 1, -1)) );
                    if ('},' == $match[3])
                        return $offset;
                } else
                    $offset = $this->pg_array_parse($text, $output[], $limit, $offset + 1);
            }
            while ($limit > $offset);
            $i=0;
            $c=count($output)-1;
            $salida=array();
            while($i<=$c){

            $recoger[$i]=explode(",", $output[$i]);
            $salida[$i]['serial']=str_replace('{', '', $recoger[$i][0]);
            $salida[$i]['resultado']=$recoger[$i][1];
            $salida[$i]['mensaje']= Utiles::onlyTextString($recoger[$i][2]);
            $salida[$i]['mensaje']= str_replace("}", '', $salida[$i]['mensaje']);
            $i++;
            }
        return $salida;
    }
    public static function onlyTextString($input) {
        return preg_replace("/[^A-Za-z0-9 .,\-]/", "", $input);
    }
}
