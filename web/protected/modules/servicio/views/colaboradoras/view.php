<?php
/* @var $this ColaboradorasController */
/* @var $model Colaborador */

$this->breadcrumbs=array(
	'Colaboradors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Colaborador', 'url'=>array('index')),
	array('label'=>'Create Colaborador', 'url'=>array('create')),
	array('label'=>'Update Colaborador', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Colaborador', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Colaborador', 'url'=>array('admin')),
);
?>

<h1>View Colaborador #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'origen',
		'cedula',
		'fecha_nacimiento',
		'nombre',
		'apellido',
		'sexo',
		'telefono',
		'telefono_celular',
		'email',
		'twitter',
		'foto',
		'mision_id',
		'certificado_medico',
		'manipulacion_alimentos',
		'username',
		'estado_id',
		'municipio_id',
		'parroquia_id',
		'direccion',
		'enfermedades',
		'alergias',
		'observacion',
		'tipo_cuenta_id',
		'banco_id',
		'numero_cuenta',
		'origen_titular',
		'cedula_titular',
		'nombre_titular',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'estatus',
		'plantel_actual_id',
		'cant_hijos',
		'hijo_en_plantel',
		'grado_instruccion_id',
		'password',
		'busqueda',
	),
)); ?>
