<?php
/* @var $this ColaboradorasController */
/* @var $model Colaborador */
/* @var $form CActiveForm */
?>
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
        <li><a href="#asistencia" data-toggle="tab">Asistencia</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'colaborador-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div id="resultado">
                    <div class="infoDialogBox">
                        <p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p>
                    </div>
                </div>

                <?php if($model->hasErrors()):$this->renderPartial('//errorSumMsg', array('model' => $model)); endif; ?>

                <div id="general">

                    <div class="widget-box">

                        <div class="widget-header">
                            <h5>Datos de Identificación y Servicio</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-body-inner">
                                <div class="widget-main">
                                    <div class="widget-main form">

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'origen'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[origen]', $model->origen, array('V'=>'(V) Venezonalo', 'E'=>'(E) Extrangero', 'P'=>'(P) Con Pasaporte'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'cedula'); ?>
                                                    <?php echo $form->textField($model, 'cedula', array('maxlength' => "15", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'Nro. de Cédula')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                                                    <?php echo $form->textField($model, 'fecha_nacimiento', array('maxlength' => "20", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'DD-MM-YYYY')); ?>
                                                </div>
                                            </div>

                                            <div class="space-6"></div>

                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'nombre'); ?>
                                                    <?php echo $form->textField($model, 'nombre', array('maxlength' => "40", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'Nombres')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'apellido'); ?>
                                                    <?php echo $form->textField($model, 'apellido', array('maxlength' => "40", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'Apellidos')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'sexo'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[origen]', $model->sexo, array('F'=>'Femenino', 'M'=>'Masculino'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12')); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="space-6"></div>
                                            
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'mision_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[mision_id]', $model->mision_id, CHtml::listData($misiones, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12',)); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'certificado_medico'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[certificado_medico]', $model->certificado_medico, array('SI'=>'Sí', 'No'=>'No',), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'manipulacion_alimentos'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[manipulacion_alimentos]', $model->manipulacion_alimentos, array('SI'=>'Sí', 'No'=>'No',), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12')); ?>
                                                </div>
                                            </div>

                                            <div class="space-6"></div>
                                            
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'enfermedades'); ?>
                                                    <?php echo $form->textArea($model, 'enfermedades', array('maxlength' => '400', 'rows'=>'3', 'class' => 'span-12', 'placeholder'=>'Indique si padece de alguna enfermedad o alergia')); ?>
                                                </div>

                                                <div class="col-md-8">
                                                    <?php echo $form->labelEx($model,'observacion'); ?>
                                                    <?php echo $form->textArea($model, 'observacion', array('maxlength' => '400', 'rows'=>'3', 'class' => 'span-12', 'placeholder'=>'Indique si lo amerita alguna observación')); ?>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space-6"></div>

                    <div class="widget-box">

                        <div class="widget-header">
                            <h5>Datos de Contacto y de Ubicación</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-body-inner">
                                <div class="widget-main">
                                    <div class="widget-main form">

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'telefono'); ?>
                                                    <?php echo $form->textField($model, 'telefono', array('maxlength' => "14", 'class' => 'span-12', 'placeholder'=>'(0251)000-0000')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'telefono_celular'); ?>
                                                    <?php echo $form->textField($model, 'telefono_celular', array('maxlength' => "14", 'class' => 'span-12', 'placeholder'=>'(0426)000-0000')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'email'); ?>
                                                    <?php echo $form->emailField($model, 'email', array('maxlength' => "120", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'user@email.com')); ?>
                                                </div>
                                            </div>

                                            <div class="space-6"></div>

                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'estado_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[estado_id]', $model->estado_id, CHtml::listData($estados, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12',
                                                                        'ajax' => array(
                                                                            'type' => 'POST',
                                                                            'id' => 'Colaborador_estado_id',
                                                                            'update' => '#Colaborador_municipio_id',
                                                                            'url' => CController::createUrl('/servicio/colaboradoras/municipiosStandalone'),
                                                                        ),
                                                    )); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'municipio_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[municipio_id]', $model->municipio_id, CHtml::listData($municipios, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12',
                                                                        'ajax' => array(
                                                                            'type' => 'POST',
                                                                            'id' => 'Colaborador_municipio_id',
                                                                            'update' => '#Colaborador_parroquia_id',
                                                                            'url' => CController::createUrl('/servicio/colaboradoras/parroquiasStandalone'),
                                                                        ),
                                                    )); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'parroquia_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[parroquia_id]', $model->parroquia_id, CHtml::listData($parroquias, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12', )); ?>
                                                </div>
                                            </div>

                                            <div class="space-6"></div>

                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <?php echo $form->labelEx($model,'direccion'); ?>
                                                    <?php echo $form->textArea($model, 'direccion', array('maxlength' => "400", 'rows'=>'3', 'class' => 'span-12', 'placeholder'=>'Ingrese la dirección referencial de la vivienda de la Madre o Padre Colaborador')); ?>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <div class="space-6"></div>

                    <div class="widget-box">

                        <div class="widget-header">
                            <h5>Datos de Bancarios</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-body-inner">
                                <div class="widget-main">
                                    <div class="widget-main form">

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'banco_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[banco_id]', $model->banco_id, CHtml::listData($bancos, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12',)); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'tipo_cuenta_id'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[tipo_cuenta_id]', $model->tipo_cuenta_id, CHtml::listData($tiposDeCuenta, 'id', 'nombre'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12',)); ?>
                                                </div>
                                                

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'numero_cuenta'); ?>
                                                    <?php echo $form->textField($model, 'numero_cuenta', array('maxlength' => "23", 'class' => 'span-12', 'title'=>'Número de Cuenta Bancaria')); ?>
                                                </div>
                                            </div>

                                            <div class="space-6"></div>
                                            
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'origen_titular'); ?>
                                                    <?php echo CHtml::dropDownList('Colaborador[origen_titular]', $model->origen_titular, array('V'=>'(V) Venezonalo', 'E'=>'(E) Extrangero', 'P'=>'(P) Con Pasaporte'), array('prompt' => '- - -', 'required'=>'required', 'class'=>'span-12')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'cedula_titular'); ?>
                                                    <?php echo $form->textField($model, 'cedula_titular', array('maxlength' => "15", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'Nro. de Cédula del Titular de la Cuenta')); ?>
                                                </div>

                                                <div class="col-md-4">
                                                    <?php echo $form->labelEx($model,'nombre_titular'); ?>
                                                    <?php echo $form->textField($model, 'nombre_titular', array('maxlength' => "80", 'required'=>'required', 'class' => 'span-12', 'placeholder'=>'Nombre y Apellido del Titular de la Cuenta')); ?>
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <hr>
                    
                    <div class="row">

                        <div class="col-md-6">
                            <a class="btn btn-danger" href="/servicio/colaboradoras" id="btnRegresar">
                                <i class="icon-arrow-left"></i>
                                Volver
                            </a>
                        </div>

                        <div class="col-md-6 wizard-actions">
                            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                Guardar
                                <i class="icon-save icon-on-right"></i>
                            </button>
                        </div>

                    </div>
                    
                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>

        <div class="tab-pane" id="asistencia">
            <div class="alertDialogBox">
                <p>
                    Próximamente: La consulta de Asistencia de las Madres y Padres Colaboradores se encuentra en Desarrollo.
                </p>
            </div>
        </div>

    </div>
</div>