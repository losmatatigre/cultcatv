<?php
/* @var $this ColaboradorasController */
/* @var $model Colaborador */

$this->breadcrumbs=array(
	'Colaboradors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Colaborador', 'url'=>array('index')),
	array('label'=>'Create Colaborador', 'url'=>array('create')),
	array('label'=>'View Colaborador', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Colaborador', 'url'=>array('admin')),
);
?>

<h1>Update Colaborador <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>