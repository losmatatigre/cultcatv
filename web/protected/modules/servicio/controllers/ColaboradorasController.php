<?php

/**
 * Clase Controladora de Peticiones del Módulo de Gestión de registro y actualización de datos generales de Padres y Madres Colaboradores.
 *
 * @author José Gabriel González <jgonzalezp@me.gob.ve>
 * @createAt 2014-08-05
 * @updateAt 2014-08-05
 */
class ColaboradorasController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de Proveedores',
        'write' => 'Creación y Modificación de Proveedores',
        'label' => 'Activación e Inactivación de Proveedores'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'creacion', 'edicion', 'municipiosStandAlone', 'parroquiasStandAlone'),
                'pbac' => array('read', 'write', 'admin'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Manages all models.
     */
    public function actionLista() {

        $model = new Colaborador('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('Colaborador')){
            $model->attributes = $this->getQuery('Colaborador');
        }

        if(Yii::app()->user->pbac('write')){ //El permiso Write se debe dar a las autoridades de Zona Educativa
            $model->estado_id = Yii::app()->user->estado;
        }
        elseif(Yii::app()->user->pbac('read')){ //El permiso Write se debe dar a las autoridades de Zona Educativa
            $model->plantel_actual_id = AutoridadPlantel::model()->buscarPlantelAutoridadByUser(Yii::app()->user->id);
        }

        $this->render('admin', array(
            'model' => $model,
            'dataProviderColaboradores' => $model->search(),
        ));
    }

    /**
     * Displays the data model.
     * @param string $id the ID on base64_enconde of the model to be displayed
     */
    public function actionConsulta($id) {

        $idDecoded = base64_decode($id);

        $this->render('view', array(
            'model' => $this->loadModel($idDecoded),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreacion() {

        $model = new Colaborador;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if ($this->hasPost('Colaborador')) {
            $model->attributes = $this->getPost('Colaborador');
            if ($model->save()){

            }
        }

        $this->render('create', array(
            'model' => $model,
            'estados' => CEstado::getData(),
            'municipios' => array(),
            'parroquias' => array(),
            'bancos' => CBanco::getData(),
            'tiposDeCuenta' => CTipoCuenta::getData(),
            'misiones' => CMision::getData(),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Colaborador'])) {
            $model->attributes = $this->getPost('Colaborador');
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }


    public function actionMunicipiosStandAlone(){

        if($this->hasPost('Colaborador')){
            $model = new Colaborador();
            $model->attributes = $this->getPost('Colaborador');
            $this->forward($this->createUrl('/ayuda/selectCatastro/municipiosStandalone/estadoId/'.$model->estado_id.'/municipioId/'.$model->municipio_id));
        }
        else{
            throw new CHttpException(400, 'Bad Request');
        }

    }

    public function actionParroquiasStandAlone(){

        if($this->hasPost('Colaborador')){
            $model = new Colaborador();
            $model->attributes = $this->getPost('Colaborador');
            $this->forward($this->createUrl('/ayuda/selectCatastro/parroquiasStandalone/municipioId/'.$model->municipio_id.'/parroquiaId/'.$model->parroquia_id));
        }
        else{
            throw new CHttpException(400, 'Bad Request');
        }

    }

    /**
     * Performs the AJAX validation.
     * @param Colaborador $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'colaborador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function fechaNacimiento($data) {
        $fechaNac = trim($data["fecha_nacimiento"]);
        $fechaVacia = empty($fechaNac);
        if ($fechaVacia) {
            $fechaNac = "";
        } else {
            $fechaNac = date("d-m-Y", strtotime($fechaNac));
        }
        return $fechaNac;
    }

    public function sexo($data){
        $sexo = trim($data['sexo']);
        $sexoVacio = empty($sexo);
        if(!$sexoVacio){
            $sexo = strtr($sexo, array('F'=>'Femenino', 'M'=>'Masculino'));
        }
        return $sexo;
    }

    public function listaDeSexos() {
        $sexos = array();
        $sexos[] = array('id' => 'F', 'nombre' => 'Femenino');
        $sexos[] = array('id' => 'M', 'nombre' => 'Masculino');
        return $sexos;
    }

    public function columnaAcciones($data) {

        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/servicio/colaboradoras/consulta/id/'.$id)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/servicio/colaboradoras/edicion/id/'.$id)) . '&nbsp;&nbsp;';

        return $columna;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Colaborador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Colaborador::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


}
