



<div class="form-box" id="login-box">
    <div class="header bg-blue">
        UPTAMCA TV&nbsp;&nbsp;&nbsp;&nbsp;
        <img  class="pull-left" width="50" height="50" src="<?php echo Yii::app()->request->baseUrl; ?>/public/img/logo/logo_blanco.png" >
</div>
    <!--<form action="../../index.html" method="post">-->
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableAjaxValidation' => false,
        'focus' => array($model, 'username'),
    ));
    ?>
    <div class="body bg-gray">
         <div id="result">
    <?php if ($model->getErrors()): ?>
        <?php if ($model->getError('password') != '' || $model->getError('username') != ''): ?>
            <div class="callout callout-danger">
                <p>
                    Usuario o Clave incorrecta. 
                </p>
            </div>
        <?php else: ?>
            <div class="callout callout-danger">
                <p>
                    Puede que su usuario se encuentre Inactivo.
                </p>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php if (isset(Yii::app()->request->cookies['success'])): ?>
    <div class="infoDialogBox">
        <p><?php echo Yii::app()->request->cookies['success']->value; ?></p>
        <?php unset(Yii::app()->request->cookies['success']); ?>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="infoDialogBox">
        <p><?php echo Yii::app()->user->getFlash('success'); ?></p>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('mail')): ?>
    <div class="infoDialogBox">
        <p><?php echo Yii::app()->user->getFlash('mail'); ?></p>
    </div>
<?php endif; ?>
        
        <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon">
                <i class="icon-user"></i>
            </span>
            <input type="text" value="<?php echo $model->username; ?>" id="UserGroupsUser_username" name="UserGroupsUser[username]" placeholder="Usuario" required="required" class="input form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon">
                <i class="icon-lock"></i>
            </span>
                            <input type="password" value="<?php echo $model->password; ?>" id="UserGroupsUser_password" name="UserGroupsUser[password]" placeholder="Clave" required="required" class="input form-control">
            </div>
        </div>          
        <div class="form-group">
            <span class="block input-icon input-icon-right lighter">
                <?php echo $form->checkBox($model, 'rememberMe', array('id' => 'rememberMe', 'checked' => 'checked')); ?>
                <label for="rememberMe" class="lighter">Recuérdame</label>
            </span>
        </div>
       <div class="block clearfix">
                                <div class="col-xs-4" style="padding-left: 0px;">
                                    <!--<a id="" tabindex="-1" style="border-style: none;" title="Haga Click para obtener otra Imágen. El Código no es sensible a mayúsculas y minúsculas.">-->
                                        <img id="siimage" style="border: 1px solid #DDDDDD; margin-right: 15px" src="/login/captcha/sid/<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left" height="45" />
                                    <!--</a>-->
                                    <a id="linkRefreshCaptcha" tabindex="-1" style="border-style: none;" title="Haga Click para obtener otra Imágen. El Código no es sensible a mayúsculas y minúsculas.">¡Reiniciar captcha!</a>
                                </div>
                                <div class="col-xs-8" style="text-align: right; padding-right: 0px;">
                                    <span class="block input-icon input-icon-right">
                                        <?php echo $form->textField($model,'verifyCode', array('required'=>'required', 'style'=>'width: 100%;', 'maxlength'=>'10', 'required'=>'required', 'placeholder'=>'Ingrese el Código de la Imagen', 'title'=>'Ingrese el Código de la Imagen. El código no es sensible a mayúsculas y minúsculas.', 'autocomplete'=>'off', 'class'=>'form-control')); ?>
                                        <i class="icon-qrcode"></i>
                                    </span>
                                </div>
                            </div>
                            <div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div><div></div></div>
                                    <div><input type="hidden" name="<?php echo $tokenName; ?>" value="<?php echo $tokenValue; ?>" /></div>
                                    <div><div></div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                            </div>
    </div>
    <div class="footer">  
        
        <button type="submit" class=" width-35 pull-right btn btn-primary btn-block">
              <i class="icon-key"></i>
            Ingresar</button>  

        <p><a href="/recovery">¿Olvidaste tu Clave?</a></p>

    </div>

    <?php $this->endWidget(); ?>
    <!--</form>-->
</div>

