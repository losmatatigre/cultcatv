$(document).ready(function() {
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        'showOn': 'focus',
        'showOtherMonths': false,
        'selectOtherMonths': true,
        'changeMonth': true,
        'changeYear': true,
        minDate: new Date(1999, 1, 1),
        maxDate: 'today'
    });
    $('#fecha_desde').datepicker();
    $('#fecha_desdeMod').datepicker();



});
function actualizar_app()
{
    $.ajax({
        url: "App/update",
        data: $("#app-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(resp) {

            $("#div_resultado").html(resp);
            // alert(resp);



        }
    })

} //




function editar_app(id)
{

    Loading.show();

    var data = {
        id: id

    };


    $.ajax({
        url: "App/update",
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(resp) {
            $("#div_edit_create_view").html(resp);
            $("#dialogPantallaEditar").dialog("open");



        }
    })

    Loading.hide();


}

/* Se usan en Modificar Plantel */
function mostrarNer() {
    if (document.getElementById('ner').checked == true) {
        document.getElementById('divCod_plantel').style.display = 'none';
        document.getElementById('divCod_plantelNER').style.display = 'block';
        document.getElementById('divNombreNer').style.display = 'block';
        $("#Plantel_cod_plantel").val('');
        $("#Plantel_cod_plantel").attr('disable','disable');
}else{
    document.getElementById('divCod_plantel').style.display = 'block';
        document.getElementById('divCod_plantelNER').style.display = 'none';
        document.getElementById('divNombreNer').style.display = 'none';
        $("#cod_plantelNer").val('');
        $("#cod_plantelNer").attr('disable','disable');
}
}
function agregarProyecto() {
    var proyecto_endogeno_id = $("#proyectos_endogenos").val();
    var plantel_id = $("#plantel_id").val();
    var data =
            {
                proyecto_endogeno_id: proyecto_endogeno_id,
                plantel_id: plantel_id
            };
    $("#dialog_agregar_proyecto span").html($("#proyectos_endogenos option:selected").text());
    var dialog = $("#dialog_agregar_proyecto").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Agregar Proyecto</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "Agregar Proyecto &nbsp; <i class='fa fa-plus icon-on-right bigger-110'></i>",
                "class": "btn btn-primary btn-xs",
                click: function() {
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');

                    $(this).dialog("close");
                }
            }
        ]
    });

    //$("#widget-endogeno").removeClass('collapsed');

}
function eliminarProyecto(proyecto_endogeno_id) {
    var plantel_id = $("#plantel_id").val();
    var data =
            {
                proyecto_endogeno_id: proyecto_endogeno_id,
                plantel_id: plantel_id,
            };
    var dialog = $("#dialog_eliminar_proyecto").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Proyecto</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Proyecto",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_formEndogeno', '../../eliminarProyecto', data, false, true, 'post', '');
                    $(this).dialog("close");
                }
            }
        ]
    });


}
function condicionarServicios() {

    $("#calidad").val('');
    $("#fecha_desde").val('');
    $("#servicio_error").addClass('hide');
    var servicio_id = $("#servicios").val();

    if (servicio_id !== '') {
        $("#servicios").val('');
        var dialog = $("#dialog_calidad_servicio").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-plus'></i> Calidad del Servicio</h4></div>",
            title_html: true,
            buttons: [
                {
                    text: "Cancelar",
                    "class": "btn btn-xs",
                    click: function() {

                        $(this).dialog("close");
                    }
                },
                {
                    text: "Agregar",
                    "class": "btn btn-primary btn-xs",
                    click: function() {
                        var mensaje = '', error = false;
                        if ($("#calidad").val() === '') {
                            mensaje = "Debe seleccionar la calidad del servicio <br>";
                            error = true;
                        }
                        if ($("#fecha_desde").val() === '') {
                            mensaje = mensaje + "Debe seleccionar la fecha desde que se esta prestando este servicio <br>";
                            error = true;
                        }
                        if (!error) {
                            var data = {
                                id: servicio_id,
                                calidad: $("#calidad").val(),
                                fecha_desde: $("#fecha_desde").val(),
                                plantel_id: $("#plantel_id").val()
                            };
                            // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                            executeAjax('_formServicio', '../../agregarServicio', data, true, true, 'get', '');
                            $("#servicios").val('');
                            $(this).dialog("close");
                        }
                        else {
                            $("#servicio_error p").html(mensaje);
                            $("#servicio_error").removeClass('hide');
                        }
                    }
                }
            ]
        });
    }
}
function eliminarServicio(id) {
    var dialog = $("#dialog_eliminar_servicio").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Servicio</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Servicio",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_formServicio', '../../eliminarServicio', data, true, true, 'get', '');
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function actualizarServicio(servicio_id) {
    $("#servicios").val('');
    $("#calidadMod").val('');
    $("#fecha_desdeMod").val('');
    $("#servicio_errorMod").addClass('hide');
    var dialog = $("#dialog_modificar_servicio").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-plus'></i> Actualizar Servicio</h4></div>",
        title_html: true,
        buttons: [
            {
                text: "Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                text: "Actualizar",
                "class": "btn btn-primary btn-xs",
                click: function() {
                    var mensaje = '', error = false;
                    if ($("#calidadMod").val() === '') {
                        mensaje = "Debe seleccionar la calidad del servicio <br>";
                        error = true;
                    }
                    if ($("#fecha_desdeMod").val() === '') {
                        mensaje = mensaje + "Debe seleccionar la fecha desde que se esta prestando este servicio <br>";
                        error = true;
                    }
                    if (!error) {
                        var data = {
                            id: servicio_id,
                            calidad: $("#calidadMod").val(),
                            fecha_desde: $("#fecha_desdeMod").val(),
                            plantel_id: $("#plantel_id").val()
                        };
                        // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                        executeAjax('_formServicio', '../../actualizarServicio', data, true, true, 'get', '');
                        $(this).dialog("close");
                    }
                    else {
                        $("#servicio_errorMod p").html(mensaje);
                        $("#servicio_errorMod").removeClass('hide');
                    }
                }
            }
        ]
    });
}
/* Se usan en Modificar Plantel */

function consultarPlantel(id) {

    direccion = 'consultar/consultarPlantel';
    title = 'Consulta de plantel';

    Loading.show();
    var data =
            {
                id: id
            };
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                title_html: true,
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}
