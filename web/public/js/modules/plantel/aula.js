
$('#Aula_nombre').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true);
});
$('#Aula_observacion').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true);
});
$('#Aula_capacidad').bind('keyup blur', function() {
    if($("#Aula_capacidad").val() > 50)
    {
        $("#Aula_capacidad").val("50");
    }
    keyNum(this, false);// true acepta la ñ y para que sea español
});
$('#Aula_nombre').bind('blur', function() {
    clearField(this);
});
$('#Aula_observacion').bind('blur', function() {
    clearField(this);
});


$('#Aula_nombre_form').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true);
});
$('#Aula_capacidad_form').bind('keyup blur', function() {
    if($("#Aula_capacidad_form").val() > 50)
    {
        $("#Aula_capacidad_form").val("50");
    }
    keyNum(this, false);// true acepta la ñ y para que sea español
});
$('#Aula_nombre_form').bind('blur', function() {
    clearField(this);
});


function consultarAula(id,url) {

    direccion = url + 'informacionAula';
    title = 'Detalles del aula';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    }

                ]
            });
            $("#dialogPantallaConsultar").html(result);
        }
    });
    Loading.hide();
}

function registrarAula(id) {

    direccion = 'registrarAula';
    title = 'Crear nuevo Aula';
    Loading.show();

    var data = {id: id};

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                draggale: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            //refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            registrar();
                            // $("#nombre_credencial").val("");
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);

        }
    });
    Loading.hide();
}

function registrar()
{

    direccion = 'crearAula';
    var nombre = $('#Aula_nombre_form').val();
    var capacidad = $('#Aula_capacidad_form').val();
    var condicion_infraestructura_id = $('#Aula_condicion_infraestructura_id').val();
    var tipo_aula_id = $('#Aula_tipo_aula_id').val();
    var observaciones = $('#Aula_observacion').val();
    var plantel_id = $('#Aula_plantel_id').val();

    var data = {Aula: {
            nombre: nombre,
            capacidad: capacidad,
            condicion_infraestructura_id: condicion_infraestructura_id,
            tipo_aula_id: tipo_aula_id,
            observacion: observaciones,
            plantel_id: plantel_id,
        }};
    //alert(data);
    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
    executeAjax('dialogPantalla', direccion, data, true, true, 'POST', refrescarGrid);
    

}


function modificarAula(id) {

    direccion = 'modificarAula';
    title = 'Modificar Aula';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                draggale: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                        }
                    }
                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

function procesarCambio()
{
    direccion = 'procesarCambioAula';
    var id = $('#id').val();
    var nombre = $('#Aula_nombre_form').val();
    var capacidad = $('#Aula_capacidad_form').val();
    var condicion_infraestructura_id = $('#Aula_condicion_infraestructura_id').val();
    var tipo_aula_id = $('#Aula_tipo_aula_id').val();
    var observaciones = $('#Aula_observacion').val();
    var plantel_id = $('#Aula_plantel_id').val();

    var data = {Aula: {
            id: id,
            nombre: nombre,
            capacidad: capacidad,
            condicion_infraestructura_id: condicion_infraestructura_id,
            tipo_aula_id: tipo_aula_id,
            observacion: observaciones,
            plantel_id: plantel_id,
        }};

    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}



function eliminarAula(id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Nivel</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Aula",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        //plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', 'eliminarAula', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    });
}


function refrescarGrid() {
    var data = {
        plantel_id: $("#Aula_plantel_id").val()
    };
    $('#nivel-grid').yiiGridView('update', {
        data:data
    });
}