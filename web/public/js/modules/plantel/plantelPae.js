
    $(document).ready(function() {
        $('#razonAccion').bind('keyup blur', function() {
            makeUpper(this);
//            keyAlpha(this, true);
        });
    });
    function accion(plantel_id, value, classValue, icon, cambio_pae_activo){
        
        if(cambio_pae_activo == 'SI'){
            $("#motivoEstatus").removeClass('hide');
        }
        else{
            $("#motivoEstatus").addClass('hide');
        }
        $("#motivo_inactividad").val('');
        $("#mensajeAlertaInfo").removeClass('hide');
        $("#mensajeAlerta").addClass('hide');
        $("#mensajeAlertaExito").addClass('hide');
        $('#razonAccion').val('');
        var dialog = $("#dialogPantallaObservacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + value + " servicios del PAE </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: value + " <i class='fa fa-arrow-" + icon + " bigger-110'></i>",
                    "class": "btn btn-" + classValue + " btn-xs",
                    "id": "botonAccion",
                    click: function() {
        
                        var plantel_pae_id = $("#PlantelPae_id").val();
                        var observacion = $("#razonAccion").val();
                        var motivo = $("#motivoInactividad").val();
                        
                        if(observacion == ''){
                            $("#mensajeAlertaInfo").addClass('hide');
                            $("#mensajeAlerta").removeClass('hide');
                        }
                        else if(cambio_pae_activo == 'SI' && motivo == ''){
                            $("#mensajeAlertaInfo").addClass('hide');
                            $("#mensajeAlerta").removeClass('hide');
                        }
                        else{
//                        evt.preventDefault();
                            $.ajax({
                                url: "/planteles/modificar/accionActivar",
                                data: 'plantel_id=' + plantel_id + '&plantel_pae_id=' + plantel_pae_id + '&observacion=' + observacion + '&cambio_pae_activo=' + cambio_pae_activo + '&motivo_inactividad=' + $("#motivo_inactividad").val(),
                                dataType: 'html',
                                type: 'post',
                                dataType: 'json',
                                success: function(json) {
                                    if(json.respuesta == 1){
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        $("#dialogPantallaObservacion").dialog("close");
                                        $("#mensajeAlertaExito").removeClass('hide');
//                                        $("#btnEstatusAccion").addClass('hide');
                                        if(cambio_pae_activo == 'NO'){
                                            var pae_estatus = 'Inactivar';
                                            var className = 'danger';
                                            $("#btnEstatusAccion").attr('class', 'btn btn-danger');
                                            $("#btnEstatusAccion").attr('onclick', "accion(" + plantel_id + ", '" + pae_estatus + "', '" + className + "', 'down', 'SI');");
                                            $("#btnEstatusAccion").html('Inactivar <i class="fa fa-arrow-down"></i>');
                                            $("#PlantelPae_pae_activo").val('SI');
                                        }
                                        if(cambio_pae_activo == 'SI'){
                                            var pae_estatus = 'Activar';
                                            var className = 'success';
                                            $("#btnEstatusAccion").attr('class', 'btn btn-success');
                                            $("#btnEstatusAccion").html('Activar <i class="fa fa-arrow-up"></i>');
                                            $("#btnEstatusAccion").attr('onclick', "accion(" + plantel_id + ", '" + pae_estatus + "', '" + className + "', 'up', 'NO');");
                                            $("#PlantelPae_pae_activo").val('NO');
                                        }
                                        $("#PlantelPae_fecha_ultima_actualizacion").val(json.fecha_actualizacion);
                                    }
                                }
                                });
                        }
                    }
                }
            ]
        });
        
    }